import sys
import numpy as np
import matplotlib.pyplot as plt
import re



def formatInScientificNotation(val, valErr=None, nv=2, nve=2):
    '''
    Returns a string (<val> +- <valErr>)e<Order>
    with nv and nve digits after . for val and valErr
 
    '''

    text = ""
    sign = val/np.abs(val)
    val = np.abs(val)
    order = int(np.floor(np.log10(val)))
    valSc = val / 10**order

    if valErr != None:
        valErrSc = valErr / 10**order
        if sign<0: text = text + " -(" + format(valSc, "."+str(nv)+"f")
        else: text = text + "  (" + format(valSc, "."+str(nv)+"f")
        text = text + " +- " + format(valErrSc, "."+str(nve)+"f")
        text = text + ")e" + str(order)
    else:
        if sign<0: text = self.text + " " + format(-valSc, "."+str(nv)+"g")
        else: text = self.text + "  " + format(valSc, "."+str(nv)+"g")

    return(text)


## Define classes

class FitBox:
    '''
    Custom made fitBox
    '''

    def __init__(self):
        self.textL = ""
        self.textR = ""
        self.lenmax = 0
        
    # void add(str, float,  float, int, int)
    def add(self, var, val, valErr=None, nv=2, nve=2, scientificNotation=False):
        if '$' in var:
            latexPos = [m.start() for m in re.finditer("!", var.replace("$","!"))]
            nLatexChar = sum([ latexPos[i+1]-latexPos[i] for i in range(0,len(latexPos),2)])
        else:
            nLatexChar = 0
        self.lenmax = max(len(var)-nLatexChar, self.lenmax)
        self.textL = self.textL + var + "\n"

        if (not scientificNotation):
            if val<0: self.textR = self.textR + " " + format(val, "."+str(nv)+"f")
            else: self.textR = self.textR + "  " + format(val, "."+str(nv)+"f")
            if valErr != None: self.textR = self.textR + " +- " + format(valErr, "."+str(nve)+"f")
            self.textR = self.textR + "\n"

        else:
            self.textR = self.textR + formatInScientificNotation(val, valErr, nv, nve) + "\n"
        #    sign = val/np.abs(val)
        #    val = np.abs(val)
        #    order = int(np.floor(np.log10(val)))
        #    valSc = val / 10**order

        #    if valErr != None:
        #        valErrSc = valErr / 10**order
        #        if sign<0: self.textR = self.textR + " -(" + format(valSc, "."+str(nv)+"f")
        #        else: self.textR = self.textR + "  (" + format(valSc, "."+str(nv)+"f")
        #        self.textR = self.textR + " +- " + format(valErrSc, "."+str(nve)+"f")
        #        self.textR = self.textR + ")e" + str(order) + "\n"
        #    else:
        #        if sign<0: self.textR = self.textR + " " + format(-valSc, "."+str(nv)+"g")
        #        else: self.textR = self.textR + "  " + format(valSc, "."+str(nv)+"g")
        #        self.textR = self.textR + "\n"


    def draw(self, x=0.67, y=0.88, fontsize=10, alpha=0.8):
        plt.figtext(x=x, y=y, s=self.textL, ha="left", va="top", fontsize=fontsize,
                    backgroundcolor="white").set_bbox(dict(color='w', alpha=alpha))
        plt.figtext(x=x+0.01*(self.lenmax+1), y=y, s=self.textR, ha="left", va="top", fontsize=fontsize,
                    backgroundcolor="white").set_bbox(dict(color='w', alpha=alpha))



## Define functions

# void makeCMSPreli(double x, double y, str year, str fill, str energy)
def makeCMSPreli(x1=.13, x2=0.9, y=.91, year="", fill="",energy=""):
    yearStr, fillStr, energyStr = "","",""
    if (year!=""): yearStr=year
    #if (fill!=""):
    #    if (" " in fill): fillStr="Fills "+fill
    #    else: fillStr="Fill "+fill
    fillStr=""
    if (energy!=""): energyStr=" ("+energy+")"
    plt.figtext(x1,y,'CMS',fontsize=15,fontweight='bold',backgroundcolor='white',ha='left').set_bbox(dict(alpha=0.))
    plt.figtext(x1+0.07,y,"Preliminary",fontsize=13,style='italic',backgroundcolor='white',ha='left').set_bbox(dict(alpha=0.))
    plt.figtext(x2,y,yearStr+energyStr,fontsize=13,style='normal',backgroundcolor='white',ha='right').set_bbox(dict(alpha=0.))
    plt.figtext(x2,y+.031,fillStr,fontsize=13,style='normal',backgroundcolor='white',ha='right').set_bbox(dict(alpha=0.))


def removeOutliers_2(L, k=1):
    return(sorted(L)[k:-k])
    
def removeOutliers(L):
    return(removeOutliers_2(L, k=1))


def isInSP(ts, sep, killListTimestamp=[]):
    ## Check kill list of problematic timestamp
    if ts in killListTimestamp: return(False, "", "")
    ## Check if the timestamp is in a scan step
    found = False
    fscan = ""
    fstep = ""
    for scan in list(sep.keys()):
        for step in list(sep[scan].keys()):
            if (ts > sep[scan][step][0]) and (ts < sep[scan][step][1]):
                found = True
                fscan = scan
                fstep = step
                break
        if found: break
    return(found, fscan, fstep)


def p2xy(p):
    if p=="H": r="X"
    elif p=="V": r="Y"
    else:
        print("ERROR!!!")
        sys.exit()
    return(r)

def xy2p(xy):
    if xy=="X": r="H"
    elif xy=="Y": r="V"
    else:
        print("ERROR!!!")
        sys.exit()
    return(r)

def scan2xy(s):
    if s=="Scan_1": r="X"
    elif s=="Scan_2": r="Y"
    else:
        print("ERROR!!!")
        sys.exit()
    return(r)

def scan2p(s):
    if s=="Scan_1": r="H"
    elif s=="Scan_2": r="V"
    else:
        print("ERROR!!!")
        sys.exit()
    return(r)



def getFillNumber(vdmName):
    vdmNumber = int(vdmName[3:])
    if vdmNumber >= 2: fill = "7443"
    else: fill = "7442"
    return(fill)


def chi2Scaling(fitParams):
    for param in list(fitParams.keys()):
        if param[-3:]!="Err" or param=="chi2" or param=="ndof":
            pass
        else:
            fitParams[param] *= max(1., np.sqrt(fitParams["chi2"]/fitParams["ndof"]))
    return()


def param2latex(param):
    '''Returns the Latex expression for fitted parameters'''
    if param in ["phi"]: r = "$\\"+param+"$"
    elif param in ["chi2"]: r = "$\chi^2$"
    elif param in ["chi2/ndof"]: r = "$\chi^2$/ndof"
    else: r = param
    return(r)


def list2str(L, strForConcatenation=""):
    if len(L) == 0:
        s = ""
    else:
        s  = L[0]
        for el in L[1:]: s = s + strForConcatenation + str(el)
    return(s)


def var2varErr(var):
    v = var.split("_")[0].split(".")[0]
    part1 = list2str(var.split("_")[0].split(".")[1:], ".")
    part2 = list2str(var.split("_")[1:], "_")
    isNonEmpty1 = (part1!="")
    isNonEmpty2 = (part2!="")
    varErr = v + "Err" + isNonEmpty1*"." + part1 + isNonEmpty2*"_" + part2
    return(varErr)
