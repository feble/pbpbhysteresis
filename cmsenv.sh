#!/bin/bash

## Define path to brilconda depending on host

rebrildev="srv-s2d16"
rebrilvmlx="brilvmlx"
relxplus6="lxplus6"
relxplus7="lxplus7"

if [[ $HOSTNAME =~ $rebrildev ]]; then
    #brilcondaPath=/nfshome0/lumipro/brilconda
    brilcondaPath=/nfshome0/lumipro/brilconda3
elif [[ $HOSTNAME =~ $rebrilvmlx ]] || [[ $HOSTNAME =~ $relxplus7 ]]; then
    #brilcondaPath=/cvmfs/cms-bril.cern.ch/brilconda
    brilcondaPath=/cvmfs/cms-bril.cern.ch/brilconda3
elif [[ $HOSTNAME =~ $relxplus6 ]]; then
    #brilcondaPath=/afs/cern.ch/cms/lumi/brilconda
    brilcondaPath=/afs/cern.ch/cms/lumi/brilconda3
else
    echo "Unknown host ${HOSTNAME}"
    echo "Exit before setup!"
    return
fi

brilcondaPath_full=$(readlink -f $brilcondaPath)

## Set ROOTSYS, PYTHONPATH, LD_LIBRARY_PATH and PATH

export ROOTSYS=${brilcondaPath}/root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib
export PATH=${brilcondaPath}/bin:$PATH
unset ROOTSYS
