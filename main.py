import sys
import os
import subprocess
import json
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
mpl.style.use('classic')
from scipy.stats import sem as sem
import scipy.stats as sct
from collections import OrderedDict

import reader as rd
import utilities as ut
import fits


plt.rcParams['legend.numpoints'] = 1  # Show only one point in legend (defaut is 2)


#########################
##   GLOBAL CONSTANTS
#########################

YEAR = "2018"
ENERGY = "5.02 TeV"

# kill list of timestamps that are known to be abnormal
killListTimestamp = [ 1542140577 ]

dataDir = "data/"
dataDirScanFile = "data/scanFiles/"

scanFiles = [ "vdm1.json", "vdm2.json", "vdm3.json", "vdm4.json", "vdm5.json" ]
scanFilesExtended = [ ut.list2str(x.split("."), "_extended.") for x in scanFiles ]
#scanFilesExtended = [ "vdm1_extended.json", "vdm2.json", "vdm3.json", "vdm4.json", "vdm5.json" ]
#beamPositionMonitors = [ "arc", "DOROS" ]
#beamPositionMonitors = [ "DOROS" ]
beamPositionMonitors = [ "arc" ]
ODFilesDOROS  = [ "./data/BPMdataPerTimestep/DOROS/"  + f for f in ("ODdata_7442.csv", "ODdata_7443.csv", "ODdata_7443.csv", "ODdata_7443.csv", "ODdata_7443.csv") ]
ODFilesArc = [ "./data/BPMdataPerTimestep/arcBPM/" + f for f in ("ODdata_7442.csv", "ODdata_7443.csv", "ODdata_7443.csv", "ODdata_7443.csv", "ODdata_7443.csv") ]
#ODFiles = [ {"DOROS": ODFilesDOROS[i], "arc": ODFilesArc[i]} for i in range(len(ODFilesDOROS)) ]
#ODFiles = [ {"DOROS": ODFilesDOROS[i]} for i in range(len(ODFilesDOROS)) ]
ODFiles = [ {"arc": ODFilesArc[i]} for i in range(len(ODFilesArc)) ]

HOFile = "HOPoints.csv"    ## Obsolete, should change fct call too
LSCFile = "LSC_DOROS.json"
Q_tunes = [ {"X": 64.29, "Y": 59.30}, {"X": 64.29, "Y": 59.30}, {"X": 64.29, "Y": 59.30}, {"X": 64.29, "Y": 59.30}, {"X": 64.29, "Y": 59.30} ]
betaStar = [ 0.5, 0.5, 0.5, 0.5, 0.5 ]  # in m


###############################################
##   DEFINE HERE THE MODULES TO BE EXECUTED
###############################################

###  Beginning of GET_AVG_OD_NONSCANNING_DIR module  ###

## Get average orbit drift in the non scanning direction
#  The produced json files can then be copied to 
#  <pathToVdmFW>/VdMFramework/VisualisationScripts/code/Sxsec 
#  and read by Sxsec.py (go into the code where 'measuredTransverseOD'
#  is defined) to apply the peak-to-peak correction.
GET_AVG_OD_NONSCANNING_DIR = 0

###  End of GET_AVG_OD_NONSCANNING_DIR module  ###


###  Beginning of HYSTERESIS_ANALYSIS module  ###

##  Hysteresis analysis
##  DIFF_VS_TIME: Plots DOROS-nominal as a function of average scan step
##                including time before and after the scan
##  _0: without subtraction of linear term i.e. "not self-calibrated"
##  _1: with subtraction of linear term first

DIFF_VS_TIME = 0
HYSTERESIS_ANALYSIS_0 = 0
HYSTERESIS_ANALYSIS_1 = 1


#  Options in HYSTERESIS_ANALYSIS_0 module only:
#  Hysteresis fit
#  Affine + sine (or more complex?) function
HYSTERESIS_FIT = 0
#  Affine fit
AFFINE_FIT = 1
#  For hysteresis fit only, 1 or 3 steps
NSTEPS = 3


## Options in HYSTERESIS_ANALYSIS_1 module only:
hysteresisAnalysis1Info = {
    "fits": [ "No" ],
    #"fits": [ "Sine", "Polynomial6" ],
    "printBPMcorrelation": False
}


## Options in DIFF_VS_TIME, HYSTERESIS_ANALYSIS_0 and HYSTERESIS_ANALYSIS_1 modules
#  Define x axis
XAXIS = "nbs"  # nbs: nominal beam separation
               # spn: scan point number
               # ats: average timestamp of each scan step
#  Show title
SHOW_TITLE = False
#  Set to True (False) to do (not to do) the hysteresis/affine fit,
#  to plot fitted parameters as a function of vdM scans and
#  print their average value


## Options in HYSTERESIS_ANALYSIS_0 and HYSTERESIS_ANALYSIS_1 modules
DO_FIT = False
#  Make scan files with corrected beam position from residual
#  beam separation after subtraction of the affine term
#  from hysteresis or affine fit
MAKE_CORRECTED_SCAN_FILE = False
#  Print expected effect of "a" parameter
EFFECT_A_PARAMETER = False
#  Plot "a" vs time (hard-coded timestamp)
A_VS_TIME = False
#  Overlap all plots in 1 fig
OVERLAP_PLOT = True
#  Color list for overlap plots
#COLORS = [ "red", "orange", "gold", "olive", "chartreuse", "green", "deepskyblue", "blue", "darkviolet", "magenta", "black", "grey" ]
COLORS = [ "r", "orange", "limegreen", "deepskyblue", "m" ]


###  End of HYSTERESIS_ANALYSIS module  ###


###  Beginning of CENTROID module  ###

##  Centroid plot
CENTROID_PLOT = False
CENTROID_CORR = ""   # "", "LSC"

###  End of CENTROID module  ###



#################################################
##   GET AVERAGE OD IN NON SCANNING DIRECTION
#################################################

if (GET_AVG_OD_NONSCANNING_DIR):

    print("Offset in the non scanning direction:")

    offsetDict = {}

    for iscan, (scanFile, ODFile) in enumerate(zip(scanFiles, ODFiles)):

        vdmName = scanFile.split(".")[0]
        vdmName2 = vdmName.replace("vdm","vdM")
        tunes = Q_tunes[iscan]
        bstar = betaStar[iscan]

        ########################################
        ##  Get beam position and separations 
        ########################################

        beamPosSep = rd.getBeamPosSep(vdmName, dataDirScanFile+scanFile, ODFile, dataDir+LSCFile, dataDir+HOFile, killListTimestamp, tunes, bstar)
     
        print("")
        print(vdmName2)
        for BPM in ("DOROS", "arc"):
            if BPM not in list(offsetDict.keys()): offsetDict[BPM] = {}
            offsetDict[BPM][vdmName2] = {}
            offY = beamPosSep["offsetSepV.interpolation1D2ptsOleksii.perScan_"+BPM]["X"]
            offX = beamPosSep["offsetSepH.interpolation1D2ptsOleksii.perScan_"+BPM]["Y"]
            #offY = np.mean(beamPosSep["ODsepV.interpolation1D.perSP_"+BPM]["X"])
            #offX = np.mean(beamPosSep["ODsepH.interpolation1D.perSP_"+BPM]["Y"])
            print("  "+BPM)
            print("\t%s: %.2f" %("X", offY))
            print("\t%s: %.2f" %("Y", offX))

            offsetDict[BPM][vdmName2]["X"] = offY
            offsetDict[BPM][vdmName2]["Y"] = offX

    for BPM in ("DOROS", "arc"):
        name = "data/offsets_"+BPM+".json"
        with open(name, 'w') as f:
            json.dump(offsetDict[BPM], f)
        print(name+" has been written")



############################
##   HYSTERESIS ANALYSIS 
############################

# *****  Define functions to be used in the main of the hysteresis analysis  *****

def getGraphData(beamPosSep, scan, VAR, bpm):

    # Define general y label here, can be overwritten below
    ylabel = bpm+"-nominal beam separation [$\mu m$]"

    # Define variable name and y axis
    # var0 is the best variable used for hysteresis analysis
    if VAR=="var0":
        #key1 = "ODsep"+ut.xy2p(scan)+".interpolation1D2ptsOleksii.perSP_"+bpm
        #varName = key1
        #yaxis = beamPosSep[key1][scan]
        #yaxisErr = np.zeros_like(yaxis)
        if bpm == "DOROS":
            #key1 = "sep.avg.perSP_"+bpm+"_LSC"
            key1 = "sep.avg.perSP_"+bpm
            key2 = "BBDeflAtDOROS"
            key3 = "ODsep"+ut.xy2p(scan)+".interpolation1D2ptsOleksii.perSP_"+bpm
            #key3 = "ODsep"+ut.xy2p(scan)+".interpolation1D2pts.perSP_"+bpm
            #key3 = "ODsep"+ut.xy2p(scan)+".interpolation1D2pts.perSP_"+bpm+"_LSC"
            key4 = "sepNom"
            key1Err = ut.var2varErr(key1)
            key2Err = ut.var2varErr(key2)
            varName = key1 + " - " + key2 + " - " + key3 + " - " + key4
            yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key4][scan]) - beamPosSep[key3][scan]
            #varName = key1 + " - " + key2 + " - " + key4
            #yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key4][scan])
            yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key2Err][scan]**2 )
        if bpm == "arc":
            key1 = "sep.avg.perSP_"+bpm
            key2 = "BBDefl"
            key3 = "ODsep"+ut.xy2p(scan)+".interpolation1D2ptsOleksii.perSP_"+bpm
            key4 = "sepNom"
            key1Err = ut.var2varErr(key1)
            key2Err = ut.var2varErr(key2)
            #varName = key1 + " - " + key2 + " - " + key3 + " - " + key4
            #yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key4][scan]) - beamPosSep[key3][scan]
            varName = key1 + " - " + key2 + " - " + key4
            yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key4][scan])
            yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key2Err][scan]**2 )


    ## For diff vs time plots
    elif VAR=="diff":
        key1 = "sep.avg.perSP_"+bpm
        key2 = "BBDefl"
        key3 = "sepNom"
        key1Err = ut.var2varErr(key1)
        key2Err = ut.var2varErr(key2)
        varName = key1 + " - " + key2 + " - " + key3

        next1 = 0
        stopCounting = False
        for x in beamPosSep[key3][scan]:
            if x == 0 and not stopCounting: next1+=1
            else: stopCounting = True

        next2 = len(beamPosSep[key1][scan]) - len(beamPosSep[key2][scan]) - next1

        ext1 = [0.] * next1
        ext2 = [0.] * next2

        arr2 = np.array( ext1 + list(beamPosSep[key2][scan]) + ext2)
        arrErr2 = np.array( ext1 + list(beamPosSep[key2Err][scan]) + ext2)

        yaxis = 1000*(beamPosSep[key1][scan] - arr2 - beamPosSep[key3][scan])
        yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + arrErr2**2 )

    elif VAR=="linearOD":
        key1 = "ODsep"+ut.xy2p(scan)+".interpolation1D2ptsOleksii.perSP_"+bpm
        yaxis = beamPosSep[key1][scan]
        #key1 = "ODsep"+ut.xy2p(scan)+".interpolation1D2pts.perSP_"+bpm
        #yaxis = 1000*beamPosSep[key1][scan]
        yaxisErr = 0
        varName = key1


    # Other variables here for bookkeeping, convenience and tests
    elif VAR=="var1":
        key1 = "sep.avg.perSP_"+bpm+"_LSC"
        key2 = "ODsep.interpolation1D.perSP"+ut.xy2p(scan)+"_DOROS_LSC"
        key3 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " - " + key2 + " - " + key3
        yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key3][scan]) - beamPosSep[key2][scan]
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var2":
        key1 = "sep.avg.perSP_"+bpm+"_LSC"
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " - " + key2
        yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var2.1":
        key1 = "sep.avg.perSP_"+bpm
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " - " + key2
        yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var2.2":
        if bpm == "DOROS":
            key1 = "sep.avg.perSP_"+bpm+"_LSC"
            key2 = "BBDefl"
            key3 = "sepNom"
            key1Err = ut.var2varErr(key1)
            key2Err = ut.var2varErr(key2)
            varName = key1 + " - " + key2 + " - " + key3
            yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key3][scan])
            yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key2Err][scan]**2 )
        if bpm == "arc":
            key1 = "sep.avg.perSP_"+bpm
            key3 = "sepNom"
            key1Err = ut.var2varErr(key1)
            varName = key1 + " - " + key3
            yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key3][scan])
            yaxisErr = 1000 * beamPosSep[key1Err][scan]**2

    elif VAR=="var2.3":
        key1 = "sep.avg.perSP_"+bpm
        key2 = "BBDefl"
        key3 = "sepNom"
        key1Err = ut.var2varErr(key1)
        key2Err = ut.var2varErr(key2)
        varName = key1 + " - " + key2 + " - " + key3
        yaxis = 1000 * (beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key3][scan])
        yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key2Err][scan]**2 )

    elif VAR=="var2.4":
        key1 = "sep.avg.perSP_"+bpm+"_LSC"
        key2 = "BBDefl"
        key3 = "ODsep"+ut.xy2p(scan)+".interpolation1D.perSP_"+bpm+"_LSC"
        key4 = "sepNom"
        key1Err = ut.var2varErr(key1)
        key2Err = ut.var2varErr(key2)
        varName = key1 + " - " + key2 + " - " + key3 + " - " + key4
        yaxis = 1000*(beamPosSep[key1][scan] - beamPosSep[key2][scan] - beamPosSep[key4][scan]) - beamPosSep[key3][scan]
        yaxisErr = 1000 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key2Err][scan]**2 )

    elif VAR=="var3":
        key1 = "B1"+ut.xy2p(scan)+".avg.perSP_"+bpm+"_LSC"
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " - 0p5" + key2
        yaxis = 1000*(beamPosSep[key1][scan] - 0.5*beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var3.1":
        key1 = "B1"+ut.xy2p(scan)+".avg.perSP_"+bpm
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " - 0p5" + key2
        yaxis = 1000*(beamPosSep[key1][scan] - 0.5*beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var4":
        key1 = "B2"+ut.xy2p(scan)+".avg.perSP_"+bpm+"_LSC"
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " + 0p5" + key2
        yaxis = 1000*(beamPosSep[key1][scan] + 0.5*beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var4.1":
        key1 = "B2"+ut.xy2p(scan)+".avg.perSP_"+bpm
        key2 = "sepNom"
        key1Err = ut.var2varErr(key1)
        varName = key1 + " + 0p5" + key2
        yaxis = 1000*(beamPosSep[key1][scan] + 0.5*beamPosSep[key2][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="var5":
        key1 = "BBDefl"
        key1Err = ut.var2varErr(key1)
        varName = key1
        yaxis = 1000*(beamPosSep[key1][scan])
        yaxisErr = 1000*beamPosSep[key1Err][scan]

    elif VAR=="centroid":
        key1 = "B1"+ut.xy2p(scan)+".avg.perSP_"+bpm
        key2 = "B2"+ut.xy2p(scan)+".avg.perSP_"+bpm
        key1Err = ut.var2varErr(key1)
        key2Err = ut.var2varErr(key2)
        varName = key1 + " + " + key2
        yaxis = 1000 * 0.5 * (beamPosSep[key1][scan] + beamPosSep[key2][scan])
        yaxisErr = 1000 * 0.5 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key1Err][scan]**2 )
        ylabel = "Beams avg. pos. B1+B2/2 [$\mu m$]"

    elif VAR=="centroid_LSC":
        key1 = "B1"+ut.xy2p(scan)+".avg.perSP_"+bpm+"_LSC"
        key2 = "B2"+ut.xy2p(scan)+".avg.perSP_"+bpm+"_LSC"
        key1Err = ut.var2varErr(key1)
        key2Err = ut.var2varErr(key2)
        varName = key1 + " + " + key2
        yaxis = 1000 * 0.5 * (beamPosSep[key1][scan] + beamPosSep[key2][scan])
        yaxisErr = 1000 * 0.5 * np.sqrt( beamPosSep[key1Err][scan]**2 + beamPosSep[key1Err][scan]**2 )      
        ylabel = "Beams avg. pos. B1+B2/2 [$\mu m$]"

    else:
        print("Unknown variable %s" %VAR)
        print("Exit!")
        sys.exit()

    # Define x axis
    if XAXIS == "spn":
        xaxis = np.array([ i for i in range(len(yaxis))])
        xlabel = "Scan point number"
        plt.xlim((-1, len(xaxis)))
        scale = 1.
    elif XAXIS == "nbs":
        xaxis = 1000*beamPosSep["sepNom"][scan]
        xlabel = "Nominal beam separation [$\mu m$]"
        scale = (len(xaxis)-1)/(max(xaxis)-min(xaxis))
    elif XAXIS == "ats":
        xaxis = beamPosSep["timestamp.avg.perSP"][scan]
        xlabel = "Time [s]"
        minx = min(xaxis)
        maxx = max(xaxis)
        delta = maxx-minx
        scale = (len(xaxis)-1)/delta
        plt.xlim( round(minx-delta/15), round(maxx+delta/15) )
    else:
        print("Unknown parameter %s for constant XAXIS" %XAXIS)
        print("Exit!")
        sys.exit()

    return(varName, xaxis, yaxis, yaxisErr, xlabel, ylabel, scale)


def drawGraph(varName, xaxis, yaxis, yaxisErr, xlabel, ylabel):
    '''Draw graph "uncorrected beam separation = f(beam separation)" '''

    if (SHOW_TITLE): plt.title(vdmName + " " + scan + "\n" + var)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()

    ut.makeCMSPreli(year=YEAR, energy=ENERGY)

    ## Plot data
    midStep = int(len(yaxis)//2)
    plt.errorbar(xaxis, yaxis, yerr=yaxisErr, fmt="ko", markersize=5)
    plt.errorbar( [xaxis[midStep]], [yaxis[midStep]], yerr=[yaxisErr[midStep]], fmt="ro", markersize=5)

    return(0)


def drawGraphLite(xaxis, yaxis, xaxisErr, yaxisErr, label, color="r", marker="o", markersize=5, linestyle=""):
    '''Draw a graph'''
    if marker=="None" or marker=="": markeredgewidth = 0
    else: markeredgewidth = 0.4
    plt.errorbar(xaxis, yaxis, xerr=xaxisErr, yerr=yaxisErr, label=label, color=color, marker=marker, markersize=markersize, linestyle=linestyle, markeredgewidth=markeredgewidth)
    return(0)



def drawHorizontalLine(y=0, color="k", ls="-"):
    '''Draw an horizontal at y=y and color=color'''
    
    x1, x2 = plt.xlim()
    plt.plot([x1, x2], [y, y], color=color, ls=ls)

    return(0)



def drawPrintFittedParameters(fitParams):

    # Print out and draw fitted parameters
    fitBoxTextL=""
    fitBoxTextR=""
    paramsList = sorted(list(fitParams.keys()))

    fitBox = ut.FitBox()
    for param in paramsList:
        sf = 1
        p = param
        if XAXIS == "nbs" or XAXIS == "ats":
            ## Scale factor for legibility or convenience
            if param == "a": sf = 100
            elif param == "f": sf = 1000
            else: sf = 1
            ## Scientific notation
            if param[0] == "p" and param[1:].isdigit():
                scientificNotation = True
            else: 
                scientificNotation = False

            if sf!=1: p = param + " * " + str(sf)
        if param[-3:]=="Err" or param=="chi2" or param=="ndof":
            pass
        elif param+"Err" in paramsList:
            if scientificNotation:
                print("%s\t%s" %(p, ut.formatInScientificNotation(fitParams[param], fitParams[param+"Err"], 2, 2)))
            else:
                print("%s\t%.2f +- %.2f" %(p, sf*fitParams[param], sf*fitParams[param+"Err"]))
            fitBox.add(ut.param2latex(p), sf*fitParams[param], sf*fitParams[param+"Err"], scientificNotation=scientificNotation)
        else:
            if scientificNotation:
                print("%s\t%s" %(p, ut.formatInScientificNotation(fitParams[param], None, 2, 2)))
            else:
                print("%s\t%.2f" %(p, sf*fitParams[param]))
            fitBox.add(ut.param2latex(p), sf*fitParams[param], scientificNotation=scientificNotation)
        #elif param+"Err" in paramsList:
        #    print("%s\t%.2f +- %.2f" %(param, fitParams[param], fitParams[param+"Err"]))
        #    fitBox.add(ut.param2latex(param), fitParams[param], fitParams[param+"Err"])
        #else:
        #    print("%s\t%.2f" %(param, fitParams[param]))
        #    fitBox.add(ut.param2latex(param), fitParams[param])
    #param = "chi2/ndof"
    #print("%s\t%.2f" %(param, fitParams["chi2"]/fitParams["ndof"]))
    #fitBox.add(ut.param2latex(param), fitParams["chi2"]/fitParams["ndof"])

    fitBox.draw()
    #fitBox.draw(x=0.15)

    return


def printEffectOfAParamater(fitParams):

    if XAXIS == "spn":
        deltaSep = 21.483  # step size in um
        lscCoef = fitParams["a"] / deltaSep
    elif XAXIS == "nbs":
        lscCoef = fitParams["a"]
    else:
        print("ERROR: Cannot compute effect of a parameter")
        lscCoef = 0.
    effect = lscCoef*100
    print("Expected effect due to 'a' parameter: %.2f%%" %effect)

    return


def drawFitFunction(xaxis, fitFct, color="b"):
    '''Draw fitting function.'''
    x = np.linspace(xaxis[0], xaxis[-1], 100)
    y = fitFct(x)
    plt.plot(x, y, ls="-", color=color)
    return


def getVdmScanParamList(fitResults):
    vdmNameList = sorted(list(fitResults.keys()), key=lambda a: int(a[3:]))
    scanList = sorted(list(fitResults[vdmNameList[0]].keys()))
    paramList = []
    paramList0 = list(fitResults[vdmNameList[0]][scanList[0]].keys())
    for param in paramList0:
        if param[-3:]=="Err" or param=="chi2" or param=="ndof":
            pass
        else:
            paramList.append(param)

    return(vdmNameList, scanList, paramList)



def plotFitResults(fitResults, scanList, paramList, vdmListList, vdmListNameList, pathDict, printAvg=True):
    '''Plot fit results as a function of vdm scan.'''

    avgFitResults = {}
    textAvgFitEmpty = True

    for vdmList, vdmListName in zip(vdmListList, vdmListNameList):
        print("%s: %s" %(vdmListName, vdmList))
        textAvgFit = ""
        avgFitResults[vdmListName] = {}
        for scan in scanList:
            textAvgFit = textAvgFit + "Scan " + scan
            for param in paramList:
                plt.figure()

                y = [ fitResults[vdmName][scan][param] for vdmName in vdmList ]
                ye = [ fitResults[vdmName][scan][param+"Err"] for vdmName in vdmList ]
                x = [ i+1 for i in range(len(y))]
         
                plt.title(param + " " + scan)
                plt.grid()
                plt.xticks(x, [v.replace("vdm","vdM") for v in vdmList], rotation=0)
                plt.errorbar(x, y, yerr=ye, fmt="ro")
                plt.xlim(0, len(x)+1)


                ## Extract average sine hysteresis
                if (param not in ("a", "b")):
                    # Get avg and unc.
                    yMean = np.mean(y)
                    if len(y)>1:
                        yMeanErr = sem(y)
                        uncType = "   (SEM)"
                    else:
                        yMeanErr = ye[0]
                        uncType = "   (fit unc. of the param)"
                    # Print it on plot
                    fitBox = ut.FitBox()
                    if param == "f": n=5
                    else: n=2
                    ## Scientific notation
                    if param[0] == "p" and param[1:].isdigit():
                        scientificNotation = True
                    else:
                        scientificNotation = False

                    #textAvgFit = textAvgFit + ("\tAverage %s:\t%.2f +- %.2f\n" %(param, yMean, yMeanErr))
                    if scientificNotation:
                        textAvgFit = textAvgFit + ("\tAverage %s:\t%s\n" %(param, ut.formatInScientificNotation(yMean, yMeanErr, 2, 2)))
                    else:
                        quote = "."+str(n)+"f"
                        textAvgFit = textAvgFit + ("\tAverage %s:\t" %param) + format(yMean, quote) + " +- " + format(yMeanErr, quote) + "\t" + uncType + "\n"
                    textAvgFitEmpty = False
                    fitBox.add("Avg "+param, yMean, yMeanErr, n, n, scientificNotation=scientificNotation)
                    fitBox.draw(x=0.67-(n-2)*0.02-0.035*scientificNotation)
                    # Save avg in dictionary
                    avgFitResults[vdmListName][param] = yMean
                    avgFitResults[vdmListName][param+"Err"] = yMeanErr


                ## Draw duplicate at modulo 2*pi for phi
                if param == "phi":
                   y2 = []
                   ye2 = []
                   x2 = []
                   for iyv, yv in enumerate(y):
                       delta=1.5
                       border = True
                       if yv<delta: yv2=yv+2*np.pi
                       elif yv>2*np.pi-delta: yv2=yv-2*np.pi
                       else: border = False
                       if border:
                           y2.append(yv2)
                           ye2.append(ye[iyv])
                           x2.append(x[iyv])
                   plt.errorbar(x2, y2, yerr=ye2, fmt="ro", alpha=0.5)


                ## Save plot
                if (vdmListName == "Fill 7443" or vdmListName == "Fills 7442 7443"):
                    path = pathDict[scan] + "fitResults/" + vdmListName.replace(" ", "_") + "/"
                    if not os.path.exists(path): os.makedirs(path)
                    for ext in (".pdf", ".png"):
                        figName = path+param+"_"+scan+ext
                        print("%s has been saved " %figName)
                        plt.savefig(figName)

                plt.close()

        if printAvg and not textAvgFitEmpty: print(textAvgFit)
        if textAvgFitEmpty: print("")

    return(avgFitResults)


def plotAvsTime(fitResults, scanList, paramList, vdmList, pathDict):
    '''
    Plot "a" parameter of the fit as a function of vdM scan time.
    Timestamp for vdM scans are hard-coded below.

    '''

    print("\nPlot 'a' parameter versus time:")
    for scan in scanList:
        if "a" in paramList:
            param = "a"
        else:
            print("WARNING: \"a\" parameter not in fitted parameters")
            print("         Cannot plot \"a\" versus time")
            return(1)

        plt.figure()

        y = [ fitResults[vdmName][scan][param] for vdmName in vdmList ]
        ye = [ fitResults[vdmName][scan][param+"Err"] for vdmName in vdmList ]
        # hard-coded timestamps
        if scan == "X": x = [ 1542214176, 1542215735, 1542222251, 1542228678 ]
        if scan == "Y": x = [ 1542214760, 1542216295, 1542222815, 1542229258 ]
 
        plt.title(param + " " + scan)
        plt.xlabel("Timestamp")
        plt.grid()
        plt.errorbar(x, y, yerr=ye, fmt="ro")

        ## Save plot
        path = pathDict[scan]+"fitResults/"
        if not os.path.exists(path): os.makedirs(path)
        for ext in (".pdf", ".png"):
            figName = path+param+"VStime_"+scan+ext
            print("%s has been saved " %figName)
            plt.savefig(figName)

        plt.close()

    return(0)


def removeZeros(arrayForMask, arrays):
    mask = []
    strippedArrayMask = []
    for ix, x in enumerate(arrayForMask):
        xIsZero = (abs(x) <= 1e-12)
        if (not xIsZero) or (xIsZero and abs(ix-len(arrayForMask)//2) < 3):
            strippedArrayMask.append(x)
            mask.append(1)
        else: 
            mask.append(0)

    strippedArrays = []
    for array in arrays:
        strippedArray = []
        for ix, x in enumerate(array):
            if mask[ix] == 1: strippedArray.append(x)
        strippedArrays.append(np.array(strippedArray))
    return(strippedArrays)



# *****  Main for the DIFF_VS_TIME analysis  *****

if DIFF_VS_TIME:

    ## Get variables to fit
    bashCommand = 'grep "VAR" var.dat | cut -d" " -f2'
    process = subprocess.run(bashCommand, shell=True, stdout=subprocess.PIPE)
    VAR = process.stdout.decode("utf-8").replace("\n","")

    # Book dictionaries to store fitResults and output paths
    # over the different fits, beam positions monitors and scans
    pathDict = {}

    ## Loop over all BPMs
    for bpm in beamPositionMonitors:
        print("Make graphs and fits for %s BPM\n" %bpm)

        pathDict[bpm] = {}

        #if (OVERLAP_PLOT):
        if (False):
            overlapPlots = {}
            pathDictOP = {}
            for scan in ("X", "Y"):
                overlapPlots[scan] = { "plots": [] }

        ## Loop over all scans
        for iscan, (scanFile, ODFile) in enumerate(zip(scanFilesExtended, ODFiles)):

            vdmName = scanFile[:4]
            tunes = Q_tunes[iscan]
            bstar = betaStar[iscan]

            ## Get beam position and separations 
            beamPosSep = rd.getBeamPosSep(vdmName, dataDirScanFile+scanFile, {bpm: ODFile[bpm]}, dataDir+LSCFile, dataDir+HOFile, killListTimestamp, tunes, bstar)
     
            ## Make graph and fit
            for scan in ("X", "Y"):

                plt.figure()

                ## Draw BPM-nominal vs time with data before and after the scan
                varName, xaxisData, yaxisData, yaxisErrData, xlabel, ylabel, scale = getGraphData(beamPosSep, scan, "diff", bpm)
                #if vdmName == "vdm1":
                #    print("discarding first 2 steps for vdM1")
                #    xaxis = xaxis[2:]
                #    yaxis = yaxis[2:]
                #    yaxisErr = yaxisErr[2:]
                outputPath = "./fig/diffVStime/"+varName.replace(" ","").replace("_"+bpm,"") + "/" + bpm + "/"
                if not os.path.exists(outputPath): os.makedirs(outputPath)


                ## Draw linear fit
                if True:
                #if False:
                    xaxisDataStripped, yaxisDataStripped, yaxisErrDataStripped = removeZeros(beamPosSep["sepNom"][scan], (xaxisData, yaxisData, yaxisErrData))
                    ## Fit with a*x+b
                    fitParams, fitFct = fits.doAffineFit(xaxisDataStripped, yaxisDataStripped, yaxisErrDataStripped)
                    drawFitFunction(xaxisDataStripped, fitFct)
                    

                ## Draw BPM-nominal vs time with data before and after the scan
                drawGraphLite(xaxisData, yaxisData, 0, yaxisErrData, "data", color="b", marker="o", markersize=5)

                ## x and y label
                plt.xlabel(xlabel)
                plt.ylabel(ylabel)


                ## Grid and CMS Preliminary
                plt.grid()
                ut.makeCMSPreli(year=YEAR, energy=ENERGY)


                ## Draw linear Orbit Drift
                varName, xaxisOD, yaxisOD, yaxisErrOD, xlabel, ylabel, scale = getGraphData(beamPosSep, scan, "linearOD", bpm)
                drawGraphLite(xaxisOD, yaxisOD, 0, yaxisErrOD, "Libear OD", color="r", marker="None", markersize=5, linestyle="--")


                ## Save plot
                path = outputPath
                if scan not in list(pathDict[bpm].keys()): pathDict[bpm][scan] = path
                if not os.path.exists(path): os.makedirs(path)
                for ext in (".pdf", ".png"):
                    figName = path+vdmName+"_"+scan+ext
                    print("%s has been saved " %figName)
                    plt.savefig(figName)
                plt.close()

 
                print("")



# *****  Main for the hysteresis analysis  *****

if (HYSTERESIS_ANALYSIS_0 and (HYSTERESIS_FIT or AFFINE_FIT)):

    ## Get variables to fit
    bashCommand = 'grep "VAR" var.dat | cut -d" " -f2'
    process = subprocess.run(bashCommand, shell=True, stdout=subprocess.PIPE)
    VAR = process.stdout.decode("utf-8").replace("\n","")

    # Book dictionaries to store fitResults and output paths
    # over the different fits, beam positions monitors and scans
    fitResults = {}
    pathDict = {}

    for hysteresisFit, affineFit in ((HYSTERESIS_FIT, False), (False, AFFINE_FIT)):

        print(hysteresisFit, affineFit)

        if hysteresisFit or affineFit:
            if hysteresisFit:
                print("\n*** Doing hysteresis fit...\n")
                fitName = "AffineSine"+str(NSTEPS)+"Step"+(NSTEPS>1)*"s"
            if affineFit:
                print("\n*** Doing affine fit...\n")
                fitName = "Affine"

            fitResults[fitName] = {}
            pathDict[fitName] = {}

            ## Loop over all BPMs
            for bpm in beamPositionMonitors:
                print("Make graphs and fits for %s BPM\n" %bpm)

                pathDict[fitName][bpm] = {}
                fitResults[fitName][bpm] = {}

                if (OVERLAP_PLOT):
                    overlapPlots = {}
                    pathDictOP = {}
                    for scan in ("X", "Y"):
                        overlapPlots[scan] = { "plots": [] }

                ## Loop over all scans
                for iscan, (scanFile, ODFile) in enumerate(zip(scanFiles, ODFiles)):

                    xaxisDict = {}   # To store x axis in the 2 scanning directions
                    yaxisDict = {}   # To store y axis in the 2 scanning directions
                    fitFctDict = {}  # To store fit function in the 2 scanning directions

                    vdmName = scanFile.split(".")[0]
                    fitResults[fitName][bpm][vdmName] = {}
                    tunes = Q_tunes[iscan]
                    bstar = betaStar[iscan]

                    ## Get beam position and separations 
                    beamPosSep = rd.getBeamPosSep(vdmName, dataDirScanFile+scanFile, {bpm: ODFile[bpm]}, dataDir+LSCFile, dataDir+HOFile, killListTimestamp, tunes, bstar)
             
                    ## Make graph and fit
                    for scan in ("X", "Y"):
                        varName, xaxis, yaxis, yaxisErr, xlabel, ylabel, scale = getGraphData(beamPosSep, scan, VAR, bpm)
                        #if vdmName == "vdm1":
                        #    print("discarding first 2 steps for vdM1")
                        #    xaxis = xaxis[2:]
                        #    yaxis = yaxis[2:]
                        #    yaxisErr = yaxisErr[2:]
                        xaxisDict[scan] = xaxis
                        yaxisDict[scan] = yaxis
                        outputPath = "./fig/hysteresisAnalysis0/"+varName.replace(" ","").replace("_"+bpm,"") + "/" + bpm + "/" + fitName + "Fit/"
                        if (OVERLAP_PLOT):
                            if scan not in list(pathDictOP.keys()):
                                pathDictOP[scan] = "./fig/hysteresisAnalysis0/"+varName.replace(" ","").replace("_"+bpm,"") + "/" + bpm + "/"
                                if "xlabel" not in list(overlapPlots[scan].keys()):
                                    overlapPlots[scan]["xlabel"] = xlabel
                                if "ylabel" not in list(overlapPlots[scan].keys()):
                                    overlapPlots[scan]["ylabel"] = ylabel
                                
                        if not os.path.exists(outputPath): os.makedirs(outputPath)

                        plt.figure()
                        drawGraph(varName, xaxis, yaxis, yaxisErr, xlabel, ylabel)

                        if (OVERLAP_PLOT):
                            overlapPlots[scan]["plots"].append({
                                "xaxis": xaxis,
                                "yaxis": yaxis,
                                "xaxisErr": 0,
                                "yaxisErr": yaxisErr,
                                "label": vdmName.replace("vdm", "vdM"),
                                "color": COLORS[len(overlapPlots[scan]["plots"])]
                                })

                        if (DO_FIT):
                            if (hysteresisFit):
                                ## Fit with a*x+b + c*sin(f*x+phi)
                                fitParams, fitFct = fits.doAffineSineFit(xaxis, yaxis, yaxisErr, NSTEPS, scale)
                            if (affineFit):
                                ## Fit with a*x+b
                                fitParams, fitFct = fits.doAffineFit(xaxis, yaxis, yaxisErr)
                            fitFctDict[scan] = fitFct

                            # Scale up stat error using reduced chi2
                            #print("Scale up stat error using reduced chi2")
                            ut.chi2Scaling(fitParams) 

                            drawPrintFittedParameters(fitParams)
                            drawFitFunction(xaxis, fitFct)
                            if (EFFECT_A_PARAMETER): printEffectOfAParamater(fitParams)

                            # Save fit results
                            fitResults[fitName][bpm][vdmName][scan] = fitParams

                        ## Save plot
                        path = outputPath
                        if scan not in list(pathDict[fitName][bpm].keys()): pathDict[fitName][bpm][scan] = path
                        if not os.path.exists(path): os.makedirs(path)
                        for ext in (".pdf", ".png"):
                            figName = path+vdmName+"_"+scan+ext
                            print("%s has been saved " %figName)
                            plt.savefig(figName)
                        plt.close()

         
                    ## Make corrected scan files
                    if ((affineFit or hysteresisFit) and MAKE_CORRECTED_SCAN_FILE):
                        #print("Make corrected scan file")

                        # Get vdm scan data
                        with open(dataDirScanFile+scanFile, 'r') as f:
                            scanInfo = json.load(f)

                        # Make corrected scan file
                        for i, name in enumerate(scanInfo["ScanNames"]):
                            key = "Scan_" + str(i+1)
                            scanpoints = scanInfo[key]
                            if key=="Scan_1":
                                xy = "X"
                                shift = 0
                            elif key=="Scan_2":
                                xy = "Y"
                                shift = 1
                            #affineArray = fitFctDict[xy](xaxisDict[xy])
                            for j, sp in enumerate(scanpoints):
                                #scanInfo[key][j][5]       += 0.001 * (yaxisDict[xy][j] - affineArray[j])  # 0.001 to put in mm
                                #scanInfo[key][j][6+shift] += 0.001 * (yaxisDict[xy][j] - affineArray[j])
                                scanInfo[key][j][5]       += 0.001 * yaxisDict[xy][j]
                                scanInfo[key][j][6+shift] += 0.001 * yaxisDict[xy][j]

                        #fileName = dataDirScanFile + scanFile.split(".")[0] + "_" + varName.replace(" ", "") + "_affineSubtraction_from" + fitName + "Fit.json"
                        fileName = dataDirScanFile + scanFile.split(".")[0] + "_" + varName.replace(" ", "") + ".json"
                        with open(fileName, 'w') as f:
                            json.dump(scanInfo, f)
                        print(fileName+" has been written")

                    print("")

                ## Making overlap plot
                if (OVERLAP_PLOT):
                    for scan in ("X", "Y"):
                        plt.figure()
                        for x in overlapPlots[scan]["plots"]:
                            drawGraphLite(x["xaxis"], x["yaxis"], x["xaxisErr"], x["yaxisErr"], x["label"], color=x["color"])
                        plt.legend()

                        plt.grid()
                        plt.xlabel(overlapPlots[scan]["xlabel"])
                        plt.ylabel(overlapPlots[scan]["ylabel"])
                        
                        ut.makeCMSPreli(year=YEAR, energy=ENERGY)

                        # Save plot
                        path = pathDictOP[scan]
                        if not os.path.exists(path): os.makedirs(path)
                        for ext in (".pdf", ".png"):
                            figName = path+"overlap_"+scan+ext
                            print("%s has been saved " %figName)
                            plt.savefig(figName)
                        plt.close()



    ## Plot fitted parameters as a function of vdM scans
    if (DO_FIT):
        fitList = list(fitResults.keys())
        bpmList = list(fitResults[fitList[0]].keys())
        for fit in fitList:
            for bpm in bpmList:
                fr = fitResults[fit][bpm]
                pathd = pathDict[fit][bpm]
                # Get the list of vdm names, scans and parameters
                # assuming same parameters for all vdms scans
                vdmNameList, scanList, paramList = getVdmScanParamList(fr)
                # Make a list of sets of vdM scans for which to compute average fitted parameters
                vdmListList = [ [vdmNameList[0]], vdmNameList[1:], vdmNameList[:] ]
                vdmListNameList = [ "Fill 7442", "Fill 7443", "Fills 7442 7443" ]
                # Plot fit results and compute averages
                print("Compute average fit results for different vdM scan lists\n")
                avgFitResults = plotFitResults(fr, scanList, paramList, vdmListList, vdmListNameList, pathd, printAvg=True)

                if (A_VS_TIME):
                    plotAvsTime(fr, scanList, paramList, vdmNameList[1:], pathd)

           


# *****  Main for the hysteresis analysis  *****

if (HYSTERESIS_ANALYSIS_1):

    ## Get variables to fit
    bashCommand = 'grep "VAR" var.dat | cut -d" " -f2'
    process = subprocess.run(bashCommand, shell=True, stdout=subprocess.PIPE)
    VAR = process.stdout.decode("utf-8").replace("\n","")

    # Book dictionaries to store fitResults and output paths
    # over the different beam positions monitors and scans
    fitResults = {}
    pathDict = {}
    residuals = OrderedDict()

    for fit in hysteresisAnalysis1Info["fits"]:

        fitResults[fit] = {}
        pathDict[fit] = {}

        ## Loop over all BPMs
        for bpm in beamPositionMonitors:
            print("Make graphs and fits for %s BPM\n" %bpm)

            fitResults[fit][bpm] = {}
            pathDict[fit][bpm] = {}
            residuals[bpm] = {}

            if (OVERLAP_PLOT):
                overlapPlots = {}
                pathDictOP = {}
                for scan in ("X", "Y"):
                    overlapPlots[scan] = { "plots": [] }

            ## Loop over all scans
            for iscan, (scanFile, ODFile) in enumerate(zip(scanFiles, ODFiles)):

                vdmName = scanFile.split(".")[0]
                tunes = Q_tunes[iscan]
                bstar = betaStar[iscan]
                fitResults[fit][bpm][vdmName] = {}
                residuals[bpm][vdmName] = {}
                yaxisDict = {}   # To store y axis in the 2 scanning directions

                ## Get beam position and separations 
                beamPosSep = rd.getBeamPosSep(vdmName, dataDirScanFile+scanFile, {bpm: ODFile[bpm]}, dataDir+LSCFile, dataDir+HOFile, killListTimestamp, tunes, bstar)
         
                ## Make graph and fit
                for scan in ("X", "Y"):
                    varName, xaxis0, yaxis0, yaxisErr0, xlabel, ylabel, scale = getGraphData(beamPosSep, scan, VAR, bpm)
                    #if vdmName == "vdm1":
                    #    print("discarding first 2 steps for vdM1")
                    #    xaxis0 = xaxis0[2:]
                    #    yaxis0 = yaxis0[2:]
                    #    yaxisErr0 = yaxisErr0[2:]
                    outputPath = "./fig/hysteresisAnalysis1/"+varName.replace(" ","").replace("_"+bpm,"") + "/" + bpm + "/" + fit + "Fit/"
                    if (OVERLAP_PLOT):
                        if scan not in list(pathDictOP.keys()):
                            pathDictOP[scan] = "./fig/hysteresisAnalysis1/"+varName.replace(" ","").replace("_"+bpm,"") + "/" + bpm + "/"
                            if "xlabel" not in list(overlapPlots[scan].keys()):
                                overlapPlots[scan]["xlabel"] = xlabel
                            if "ylabel" not in list(overlapPlots[scan].keys()):
                                overlapPlots[scan]["ylabel"] = ylabel
                                
                    if not os.path.exists(outputPath): os.makedirs(outputPath)

                    ## Info for scan file
                    yaxisDict[scan] = yaxis0

                    ## Fit with a*x + b to calibrate out the linear term in the 
                    fitParamsAffine, fitFctAffine = fits.doAffineFit(xaxis0, yaxis0, yaxisErr0)
                    xaxis = xaxis0
                    affineArray = fitFctAffine(xaxis0)
                    yaxis = yaxis0 - affineArray
                    yaxisErr = yaxisErr0
                    if (EFFECT_A_PARAMETER): printEffectOfAParamater(fitParamsAffine)

                    # Save residuals for this bpm and scan
                    residuals[bpm][vdmName][scan] = yaxis

                    plt.figure()
                    drawGraph(varName, xaxis, yaxis, yaxisErr, xlabel, ylabel)
                    if fit == "No": color = "b"
                    else: color = "k"
                    drawHorizontalLine(y=0, color=color, ls="-")

                    if (OVERLAP_PLOT):
                        overlapPlots[scan]["plots"].append({
                            "xaxis": xaxis,
                            "yaxis": yaxis,
                            "xaxisErr": 0,
                            "yaxisErr": yaxisErr,
                            "label": vdmName.replace("vdm", "vdM"),
                            "color": COLORS[len(overlapPlots[scan]["plots"])]
                            })

                    ## Fit with c*sin(f*x+phi)
                    if fit != "No":
                        fitParams, fitFct = eval("fits.do"+fit+"Fit(xaxis, yaxis, yaxisErr, scale)")

                        # Scale up stat error using reduced chi2
                        #print("Scale up stat error using reduced chi2")
                        ut.chi2Scaling(fitParams) 

                        drawPrintFittedParameters(fitParams)
                        drawFitFunction(xaxis, fitFct)

                        # Save fit results
                        fitResults[fit][bpm][vdmName][scan] = fitParams

                    ## Save plot
                    path = outputPath
                    if scan not in list(pathDict[fit][bpm].keys()): pathDict[fit][bpm][scan] = path
                    if not os.path.exists(path): os.makedirs(path)
                    for ext in (".pdf", ".png"):
                        figName = path+vdmName+"_"+scan+ext
                        print("%s has been saved " %figName)
                        plt.savefig(figName)
                    plt.close()


                ## Make corrected scan files
                MAKE_CORRECTED_SCAN_FILE = True
                if (MAKE_CORRECTED_SCAN_FILE):
                    #print("Make corrected scan file")

                    # Get vdm scan data
                    with open(dataDirScanFile+scanFile, 'r') as f:
                        scanInfo = json.load(f)

                    # Make corrected scan file
                    for i, name in enumerate(scanInfo["ScanNames"]):
                        key = "Scan_" + str(i+1)
                        scanpoints = scanInfo[key]
                        if key=="Scan_1":
                            xy = "X"
                            shift = 0
                        elif key=="Scan_2":
                            xy = "Y"
                            shift = 1
                        for j, sp in enumerate(scanpoints):
                            scanInfo[key][j][5]       += 0.001 * yaxisDict[xy][j]  # 0.001 to put in mm
                            scanInfo[key][j][6+shift] += 0.001 * yaxisDict[xy][j]

                    #fileName = dataDirScanFile + scanFile.split(".")[0] + "_" + varName.replace(" ", "") + "_hysteresisAnalysis0_affineSubtraction.json"
                    fileName = dataDirScanFile + scanFile.split(".")[0] + "_" + varName.replace(" ", "") + ".json"
                    with open(fileName, 'w') as f:
                        json.dump(scanInfo, f)
                    print(fileName+" has been written")

                print("")


            ## Making overlap plot
            if (OVERLAP_PLOT):
                for scan in ("X", "Y"):
                    plt.figure()
                    for x in overlapPlots[scan]["plots"]:
                        if scan=="Y" and "vdM1" in x["label"]: continue
                        drawGraphLite(x["xaxis"], x["yaxis"], x["xaxisErr"], x["yaxisErr"], x["label"], color=x["color"])

                    plt.legend(loc="lower left", bbox_to_anchor=(0.22, 0., 0.5, 0.5))

                    plt.grid()
                    plt.xlabel(overlapPlots[scan]["xlabel"])
                    plt.ylabel(overlapPlots[scan]["ylabel"])
                    plt.ylim([-1., 1.])
                    if XAXIS == "spn":
                        plt.xlim([-1., len(overlapPlots[scan]["plots"][0]["xaxis"])])

                    ut.makeCMSPreli(year=YEAR, energy=ENERGY)

                    # Save plot
                    path = pathDictOP[scan]
                    if not os.path.exists(path): os.makedirs(path)
                    for ext in (".pdf", ".png"):
                        figName = path+"overlap_"+scan+ext
                        print("%s has been saved " %figName)
                        plt.savefig(figName)
                    plt.close()


        if (hysteresisAnalysis1Info["printBPMcorrelation"]):
            bpmListCorr = list(residuals.keys())
            if len(bpmListCorr) < 2:
                print("WARNING: Need at least 2 BPMs to compute correlations")
                print("Skipping correlation computation")
            else:
                print("Compute correlations")
                # List of pairs of any 2 bpms
                bpmPairsListCorr = []
                for i in range(len(bpmListCorr)-1):
                    bpm1 = bpmListCorr[i]
                    for j in range(i+1, len(bpmListCorr)):
                        bpm2 = bpmListCorr[j]
                    bpmPairsListCorr.append((bpm1, bpm2))

                for bpmPair in bpmPairsListCorr:
                    bpm1 = bpmPair[0]
                    bpm2 = bpmPair[1]
                    print("\nBetween %s and %s" %(bpm1, bpm2))
                    for vdmName in list(residuals[bpm1].keys()):
                        for scan in list(residuals[bpm1][vdmName].keys()):
                            correlation, pvalue = sct.pearsonr(residuals[bpm1][vdmName][scan], residuals[bpm2][vdmName][scan])
                            print("\t%s %s: %.2f" %(vdmName, scan, correlation))
                    print("")



    ## Plot fitted parameters as a function of vdM scans
    if (DO_FIT):
        fitList = list(fitResults.keys())
        bpmList = list(fitResults[fitList[0]].keys())
        for fit in fitList:
            if fit == "No": continue
            for bpm in bpmList:
                fr = fitResults[fit][bpm]
                pathd = pathDict[fit][bpm]
                # Get the list of vdm names, scans and parameters
                # assuming same parameters for all vdms scans
                vdmNameList, scanList, paramList = getVdmScanParamList(fr)
                # Make a list of sets of vdM scans for which to compute average fitted parameters
                vdmListList = [ [vdmNameList[0]], vdmNameList[1:], vdmNameList[:] ]
                vdmListNameList = [ "Fill 7442", "Fill 7443", "Fills 7442 7443" ]
                # Plot fit results and compute averages
                print("Compute average fit results for different vdM scan lists\n")
                avgFitResults = plotFitResults(fr, scanList, paramList, vdmListList, vdmListNameList, pathd, printAvg=True)



# *****  Main for the centroid plots  *****

if (CENTROID_PLOT):
    print("*** Making centroid plots ***\n")

    ## Loop over all BPMs
    for bpm in beamPositionMonitors:
        print("\nMake graphs for %s BPM\n" %bpm)

        ## Loop over all scans
        for iscan, (scanFile, ODFile) in enumerate(zip(scanFiles, ODFiles)):

            vdmName = scanFile.split(".")[0]
            tunes = Q_tunes[iscan]
            bstar = betaStar[iscan]

            ## Get beam position and separations 
            beamPosSep = rd.getBeamPosSep(vdmName, dataDirScanFile+scanFile, {bpm: ODFile[bpm]}, dataDir+LSCFile, dataDir+HOFile, killListTimestamp, tunes, bstar)
     
            ## Make graph and fit
            for scan in ("X", "Y"):
                VAR = "centroid"+ "_"*(CENTROID_CORR!="") + CENTROID_CORR
                varName, xaxis, yaxis, yaxisErr, xlabel, ylabel, scale = getGraphData(beamPosSep, scan, VAR, bpm)
                outputPath = "./fig/centroid/" + bpm + "/" + CENTROID_CORR + "/"*(CENTROID_CORR!="")
                if not os.path.exists(outputPath): os.makedirs(outputPath)

                plt.figure()
                drawGraph(varName, xaxis, yaxis, yaxisErr, xlabel, ylabel)
                ## Save plot
                if not os.path.exists(outputPath): os.makedirs(outputPath)
                for ext in (".pdf", ".png"):
                    figName = outputPath+vdmName+"_"+scan+ext
                    print("%s has been saved " %figName)
                    plt.savefig(figName)
                plt.close()


