# PbPbHysteresis

0. Create your own fork of feble/PbPbHysteresis (upper right)  

1. Clone the repository by:
   `git clone https://gitlab.cern.ch/<username>/pbpbhysteresis.git`

2. In this repository:
   * Directories:
     * data: scan files and OD data used/produced by the scripts
     * fig: plots are saved here
   * Python scripts:
     * main.py: drives computations and makes plots. A few modules can
       be activated/deactivated depending on what you specifically want
       to do
     * reader.py: reads the DOROS data (and other data from data/)
        and returns a dictionary with all data
     * fits.py: fits the data
     * utilities.py: some useful functions
   * Bash scripts:
     * cmsenv.sh: source it to setup Python3
     * run.sh: ./run.sh to run the code
     * mergeFitPlots.sh: merges fit plots for the variable in var.dat
     * mergeFitResults.sh: merges fit results plots for the variable in
       var.dat
   * other files:
     * var.dat: Defines which variable to plot and fit
 

3. How to run?    
   There are several modules to set to `True` or `False` in main.py.

   * GET\_AVG\_OD\_NONSCANNING\_DIR:   
     Get average orbit drift in the non scanning direction.     
     The produced json files can then be copied to 
     <pathToVdmFW>/VdMFramework/VisualisationScripts/code/Sxsec 
     and read by Sxsec.py (go into the code where 'measuredTransverseOD'
     is defined) to apply the peak-to-peak correction.

   * HYSTERESIS\_FIT and AFFINE_FIT:       
     Makes the hysteresis analysis:
     * either by fitting the hysteresis (HYSTERESIS\_FIT) with an affine
       function + a more complex function (e.g. sine) to get the parametrisation
       of the hysteresis (parametrised sine function) or by subtracting
       the affine term to the residual beam separation (DOROS-Nominal) to produce a
       scan file with this corrected beam separation (DOROS-Nominal-affine)
     * or by fitting the affine component only (AFFINE\_FIT) in order to subtract it
       to the residual beam separation to produce a scan file with this corrected
       beam separation (DOROS-Nominal-affine)    

     The variable to plot, fit and used in `merge*.sh` scripts must be
     defined in var.dat like:    
     VAR xxx   
     The variables are var\<j\>, with j a float. The definition of the
     variable can be inferred from main.py.     
     To set the fit to 1 step fit or 3 steps fit, go into fits.py.     
     Options of the module:
       * XAXIS     
         Define x axis     
         Possible values:
         - "nbs" (nominal beam separation)
         - "spn" (scan point number)
       * SHOW\_TITLE    
         Set to True or False to show or not show the title
       * DO\_FIT    
         Set to True (False) to do (not to do) the hysteresis/affine fit,
         to plot fitted parameters as a function of vdM scans and
         print their average value
       * MAKE\_CORRECTED\_SCAN\_FILE    
         Make scan files with corrected beam position from residual
         beam separation after subtraction of the affine term (DOROS-Nomimal-affine)
         from hysteresis or affine fit
       * EFFECT\_A\_PARAMETER    
         Print expected effect of "a" parameter
       * A\_VS\_TIME     
         Plot "a" vs time (hard-coded timestamp) 
 
     Then do:     
     $ source cmsenv.sh     
     $ ./run.sh     
     $ ./mergeFitPlots.sh    
     $ ./mergeFitResults.sh     
     and look at the produced plots.
 

4. A few warnings:
   * If you intend to use this code for other fills than 7442 and 7443,
     there might be some things hard-coded for fills 7442 and 7443.
     Please check the code.
   * It is assumed that the first scan is an X scan and that the second
     is a Y scan.

