#!/bin/bash

PLOTS_DIR=./fig
SCANS=(X Y)
BPM=(DOROS)                 # arc DOROS
ANALYSIS=hysteresisAnalysis1
FITS=(Polynomial6)  # Affine, AffineSine1Step, AffineSine3Steps, Sine, Polynomial6
VAR=`grep "VAR" var.dat | cut -d" " -f2`
FILL="Fills_7442_7443"

for bpm in ${BPM[${@}]}; do
  if [ "$VAR" == "var0" ]; then
    if [ $bpm == "DOROS" ]; then
      VARS[0]=sep.avg.perSP_LSC-BBDefl-sepNom
      VARS[1]=sep.avg.perSP_LSC-BBDefl-sepNom
    elif [ $bpm == "arc" ]; then
      VARS[0]=sep.avg.perSP-sepNom
      VARS[1]=sep.avg.perSP-sepNom
    fi
  elif [ "$VAR" == "var1" ]; then
    VARS[0]=sepDOROSperSPLSC-sepNom-ODsepH_DOROSinterpolation1D
    VARS[1]=sepDOROSperSPLSC-sepNom-ODsepV_DOROSinterpolation1D
  elif [ "$VAR" == "var2" ]; then
    VARS[0]=sep.avg.perSP_${bpm}-sepNom
    VARS[1]=sep.avg.perSP_${bpm}-sepNom
  elif [ "$VAR" == "var2.1" ]; then
    VARS[0]=sep.avg.perSP_${bpm}-sepNom
    VARS[1]=sep.avg.perSP_${bpm}-sepNom
  elif [ "$VAR" == "var2.2" ]; then
    VARS[0]=sepDOROSperSPLSC-BBDefl-sepNom
    VARS[1]=sepDOROSperSPLSC-BBDefl-sepNom
  elif [ "$VAR" == "var3" ]; then
    VARS[0]=B1_H-0p5sepNom
    VARS[1]=B1_V-0p5sepNom
  elif [ "$VAR" == "var4" ]; then
    VARS[0]=B2_H+0p5sepNom
    VARS[1]=B2_V+0p5sepNom
  elif [ "$VAR" == "var5" ]; then
    VARS[0]=BBDefl
    VARS[1]=BBDefl
  fi
  
  for fit in ${FITS[@]}; do
    fit=${fit}Fit
    cnt=-1
    for scan in ${SCANS[@]}; do
        ((cnt++))
        var=${VARS[${cnt}]}
        plotsDir1=${PLOTS_DIR}/${ANALYSIS}/${var}/${bpm}/${fit}/fitResults/${FILL}
        plotsDir2=${plotsDir1}/merged
        if [ ! -d ${plotsDir2} ]; then mkdir -p ${plotsDir2}; fi

        name=${plotsDir2}/fitResults_${scan}.png

        paramList0=""
        for file in `ls ${plotsDir1}`; do
          if [ -f ${plotsDir1}/${file} ]; then
            paramList0=${paramList0}${file:0:-5}'\n'
          fi
        done
        paramList1=`echo -e ${paramList0} | sort | uniq`
        paramList=()
        iparam=0
        for param in ${paramList1[@]}; do
          paramList[${iparam}]=${param}${scan}
          ((iparam++))
        done
        nparams=${#paramList[@]}
        cp ${plotsDir1}/${paramList[0]}.png ${plotsDir2}/tmp1.png
        for ((idx=1; idx<3 && idx<${nparams}; idx++)); do
          convert ${plotsDir2}/tmp1.png ${plotsDir1}/${paramList[${idx}]}.png +append ${plotsDir2}/tmp1.png
        done
        if [ ${nparams} -gt 3 ]; then
          cp ${plotsDir1}/${paramList[3]}.png ${plotsDir2}/tmp2.png
          for ((idx=4; idx<6 && idx<${nparams}; idx++)); do
            convert ${plotsDir2}/tmp2.png ${plotsDir1}/${paramList[${idx}]}.png +append ${plotsDir2}/tmp2.png
          done
          convert ${plotsDir2}/tmp1.png ${plotsDir2}/tmp2.png -append ${name}
        else
          mv ${plotsDir2}/tmp1.png ${name}
        fi
        if [ ${nparams} -gt 6 ]; then
          cp ${plotsDir1}/${paramList[6]}.png ${plotsDir2}/tmp3.png
          for ((idx=7; idx<9 && idx<${nparams}; idx++)); do
            convert ${plotsDir2}/tmp3.png ${plotsDir1}/${paramList[${idx}]}.png +append ${plotsDir2}/tmp3.png
          done
          convert ${plotsDir2}/tmp1.png ${plotsDir2}/tmp2.png ${plotsDir2}/tmp3.png -append ${name}
        else
          convert ${plotsDir2}/tmp1.png ${plotsDir2}/tmp2.png -append ${name}
        fi
        if [ $? -eq 0 ]; then
          echo ${name} has been saved
        fi

        #convert ${plotsDir1}/c_${scan}.png ${plotsDir1}/phi_${scan}.png ${plotsDir1}/f_${scan}.png +append ${plotsDir2}/tmp1.png
        #convert ${plotsDir1}/a_${scan}.png ${plotsDir1}/b_${scan}.png +append ${plotsDir2}/tmp2.png
#        name=${plotsDir2}/fitResults_${scan}_${FITNAME}Fit.png
#        convert ${plotsDir2}/tmp1.png ${plotsDir2}/tmp2.png -append ${name}
#        if [ $? -eq 0 ]; then
#          echo ${name} has been saved
#          #display -resize ${W}x${H} ${name}
#        fi
    done
  done
done
