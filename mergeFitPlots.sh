#!/bin/bash

PLOTS_DIR=./fig
SCANS=(X Y)
BPMS=(DOROS)   # arc DOROS
ANALYSIS=hysteresisAnalysis0  # hysteresisAnalysis0 hysteresisAnalysis1
FITS=(AffineSine3Steps)  # Affine, AffineSine3Steps, Sine, Polynomial6
VAR=`grep "VAR" var.dat | cut -d" " -f2`


for bpm in ${BPMS[@]}; do
  if [ "$VAR" == "var0" ]; then
    if [ $bpm == "DOROS" ]; then
      VARS[0]=sep.avg.perSP_LSC-BBDefl-ODsepH.interpolation1D2ptsOleksii.perSP-sepNom
      VARS[1]=sep.avg.perSP_LSC-BBDefl-ODsepV.interpolation1D2ptsOleksii.perSP-sepNom
    elif [ $bpm == "arc" ]; then
      VARS[0]=sep.avg.perSP-sepNom
      VARS[1]=sep.avg.perSP-sepNom
    fi
  elif [ "$VAR" == "var1" ]; then
    VARS[0]=sep.avg.perSP_LSC-sepNom-ODsepH.interpolation1D.perSP
    VARS[1]=sep.avg.perSP_LSC-sepNom-ODsepV.interpolation1D.perSP
  elif [ "$VAR" == "var2" ]; then
    VARS[0]=sep.avg.perSP_LSC-sepNom
    VARS[1]=sep.avg.perSP_LSC-sepNom
  elif [ "$VAR" == "var2.1" ]; then
    VARS[0]=sep.avg.perSP-sepNom
    VARS[1]=sep.avg.perSP-sepNom
  elif [ "$VAR" == "var2.2" ]; then
    VARS[0]=sep.avg.perSP_LSC-sepNom-BBDefl-sepNom
    VARS[1]=sep.avg.perSP_LSC-sepNom-BBDefl-sepNom
  elif [ "$VAR" == "var2.3" ]; then
    VARS[0]=sep.avg.perSP-sepNom-BBDefl-sepNom
    VARS[1]=sep.avg.perSP-sepNom-BBDefl-sepNom
  elif [ "$VAR" == "var2.4" ]; then
    VARS[0]=sep.avg.perSP_LSC-BBDefl-sepNom-ODsepH.interpolation1D.perSP_LSC
    VARS[1]=sep.avg.perSP_LSC-BBDefl-sepNom-ODsepV.interpolation1D.perSP_LSC
  elif [ "$VAR" == "var3" ]; then
    VARS[0]=B1H.avg.perSP-0p5sepNom
    VARS[1]=B1V.avg.perSP-0p5sepNom
  elif [ "$VAR" == "var4" ]; then
    VARS[0]=B2H.avg.perSP+0p5sepNom
    VARS[1]=B2V.avg.perSP+0p5sepNom
  elif [ "$VAR" == "var5" ]; then
    VARS[0]=BBDefl
    VARS[1]=BBDefl
  fi
  

  for fit in ${FITS[@]}; do
    cnt=-1
    for scan in ${SCANS[@]}; do
        ((cnt++))
        var=${VARS[${cnt}]}
        plotsDir1=${PLOTS_DIR}/${ANALYSIS}/${var}/${bpm}/${fit}Fit
        plotsDir2=${plotsDir1}/merged

        if [ ! -d ${plotsDir2} ]; then mkdir -p ${plotsDir2}; fi
        convert ${plotsDir1}/vdm2_${scan}.png ${plotsDir1}/vdm3_${scan}.png +append ${plotsDir2}/tmp1.png
        convert ${plotsDir1}/vdm4_${scan}.png ${plotsDir1}/vdm5_${scan}.png +append ${plotsDir2}/tmp2.png
        name=${plotsDir2}/fitPlots_${scan}.png
        convert ${plotsDir2}/tmp1.png ${plotsDir2}/tmp2.png -append ${name}
        if [ $? -eq 0 ]; then
          echo ${name} has been saved
          #display -resize ${W}x${H} ${name}
        fi
    done
  done
done
