import numpy as np
from array import array
import ROOT



#############################
#####    AFFINE FIT     ##### 
#############################

def doAffineFit(x0, y0, ye0):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine fit
    ff = ROOT.TF1("ff1","[0]*x+[1]")
    ff.SetParNames("a", "b")
    for j in range(5):
        fit = graph.Fit("ff1","SQN")

    a = ff.GetParameter("a")
    b = ff.GetParameter("b")
    aErr = ff.GetParError(ff.GetParNumber("a"))
    bErr = ff.GetParError(ff.GetParNumber("b"))

    ## Return fit parameters and fit function
    fitParams = {
        "a"  : a,
        "b"  : b,
        "aErr"  : aErr,
        "bErr"  : bErr,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: a*x+b

    return(fitParams, fitFct)



#############################################
#####    HIGH ORDER POLYNOMIALS FIT     ##### 
#############################################

def doPolynomial3Fit(x0, y0, ye0, scale):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine fit
    ff = ROOT.TF1("ff1","[0] + [1]*x + [2]*x**2 + [3]*x**3")
    ff.SetParNames("p0", "p1", "p2", "p3")
    for j in range(5):
        fit = graph.Fit("ff1","SQN")

    p0 = ff.GetParameter("p0")
    p1 = ff.GetParameter("p1")
    p2 = ff.GetParameter("p2")
    p3 = ff.GetParameter("p3")
    p0Err = ff.GetParError(ff.GetParNumber("p0"))
    p1Err = ff.GetParError(ff.GetParNumber("p1"))
    p2Err = ff.GetParError(ff.GetParNumber("p2"))
    p3Err = ff.GetParError(ff.GetParNumber("p3"))

    ## Return fit parameters and fit function
    fitParams = {
        "p0"  : p0,
        "p1"  : p1,
        "p2"  : p2,
        "p3"  : p3,
        "p0Err"  : p0Err,
        "p1Err"  : p1Err,
        "p2Err"  : p2Err,
        "p3Err"  : p3Err,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: p0 + p1*x + p2*x**2 + p3*x**3

    return(fitParams, fitFct)


def doPolynomial5Fit(x0, y0, ye0, scale):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine fit
    ff = ROOT.TF1("ff1","[0] + [1]*x + [2]*x**2 + [3]*x**3 + [4]*x**4 + [5]*x**5")
    ff.SetParNames("p0", "p1", "p2", "p3", "p4", "p5")
    for j in range(10):
        fit = graph.Fit("ff1","SQN")

    p0 = ff.GetParameter("p0")
    p1 = ff.GetParameter("p1")
    p2 = ff.GetParameter("p2")
    p3 = ff.GetParameter("p3")
    p4 = ff.GetParameter("p4")
    p5 = ff.GetParameter("p5")
    p0Err = ff.GetParError(ff.GetParNumber("p0"))
    p1Err = ff.GetParError(ff.GetParNumber("p1"))
    p2Err = ff.GetParError(ff.GetParNumber("p2"))
    p3Err = ff.GetParError(ff.GetParNumber("p3"))
    p4Err = ff.GetParError(ff.GetParNumber("p4"))
    p5Err = ff.GetParError(ff.GetParNumber("p5"))

    ## Return fit parameters and fit function
    fitParams = {
        "p0"  : p0,
        "p1"  : p1,
        "p2"  : p2,
        "p3"  : p3,
        "p4"  : p4,
        "p5"  : p5,
        "p0Err"  : p0Err,
        "p1Err"  : p1Err,
        "p2Err"  : p2Err,
        "p3Err"  : p3Err,
        "p4Err"  : p4Err,
        "p5Err"  : p5Err,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: p0 + p1*x + p2*x**2 + p3*x**3 + p4*x**4 + p5*x**5

    return(fitParams, fitFct)


def doPolynomial6Fit(x0, y0, ye0, scale):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine fit
    ff = ROOT.TF1("ff1","[0] + [1]*x + [2]*x**2 + [3]*x**3 + [4]*x**4 + [5]*x**5 + [6]*x**6")
    ff.SetParNames("p0", "p1", "p2", "p3", "p4", "p5", "p6")
    for j in range(10):
        fit = graph.Fit("ff1","SQN")

    p0 = ff.GetParameter("p0")
    p1 = ff.GetParameter("p1")
    p2 = ff.GetParameter("p2")
    p3 = ff.GetParameter("p3")
    p4 = ff.GetParameter("p4")
    p5 = ff.GetParameter("p5")
    p6 = ff.GetParameter("p6")
    p0Err = ff.GetParError(ff.GetParNumber("p0"))
    p1Err = ff.GetParError(ff.GetParNumber("p1"))
    p2Err = ff.GetParError(ff.GetParNumber("p2"))
    p3Err = ff.GetParError(ff.GetParNumber("p3"))
    p4Err = ff.GetParError(ff.GetParNumber("p4"))
    p5Err = ff.GetParError(ff.GetParNumber("p5"))
    p6Err = ff.GetParError(ff.GetParNumber("p6"))

    ## Return fit parameters and fit function
    fitParams = {
        "p0"  : p0,
        "p1"  : p1,
        "p2"  : p2,
        "p3"  : p3,
        "p4"  : p4,
        "p5"  : p5,
        "p6"  : p6,
        "p0Err"  : p0Err,
        "p1Err"  : p1Err,
        "p2Err"  : p2Err,
        "p3Err"  : p3Err,
        "p4Err"  : p4Err,
        "p5Err"  : p5Err,
        "p6Err"  : p6Err,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: p0 + p1*x + p2*x**2 + p3*x**3 + p4*x**4 + p5*x**5 + p6*x**6


    return(fitParams, fitFct)





###########################
#####    SINE FIT     ##### 
###########################

def doSineFit(x0, y0, ye0, scale=1):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine + sine fit with limits
    ff = ROOT.TF1("ff","[0]*sin(2*pi*[1]*x+[2])")
    ff.SetParameters(0.2, 0.1*scale, 3)
    ff.SetParNames("c", "f", "phi")
    ff.SetParLimits(0, 0.001, 10)  # force it to be non zero
    ff.SetParLimits(1, 0.03*scale, 1.*scale)   # force it to be non zero

    for j in range(5):
        #fit = graph.Fit("ff","SVN")
        fit = graph.Fit("ff","SQN")
        if fit.CovMatrixStatus()==3 and fit.Chi2()/fit.Ndf() < 2: break

    fitStatus = -999
    fitStatus = fit.Status()

    ## Get parameters and stat error
    c = ff.GetParameter("c")
    cErr = ff.GetParError(ff.GetParNumber("c"))
    f = ff.GetParameter("f")
    fErr = ff.GetParError(ff.GetParNumber("f"))
    phi = ff.GetParameter("phi")
    phiErr = ff.GetParError(ff.GetParNumber("phi"))

    # Impose phi in [0, 2*pi[
    #        c > 0
    # for the purpose of comparing fit results of different vdm scans
    if c<0:
        c = -c
        phi = phi + np.pi
    phi = phi%(2*np.pi)

    fitParams = {
        "c"  : c,
        "f"  : f,
        "phi": phi,
        "cErr"  : cErr,
        "fErr"  : fErr,
        "phiErr": phiErr,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: c*np.sin(2*np.pi*f*x+phi)

    return(fitParams, fitFct)




####################################
#####    AFFINE + SINE FIT     ##### 
####################################

def doAffineSineFit(x0, y0, ye0, nstep=3, scale=1):
    if nstep==3: fitParams, fitFct = doAffineSineFit3steps(x0, y0, ye0, scale)
    elif nstep==1: fitParams, fitFct = doAffineSineFit1step(x0, y0, ye0, scale)
    return(fitParams, fitFct)


def doAffineSineFit1step(x0, y0, ye0, scale=1):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

    ## Affine + sine fit with limits
    ff = ROOT.TF1("ff","([0]*x+[1]) + ([2]*sin(2*pi*[3]*x+[4]))")
    ff.SetParameters(0., 0., 0.2, 0.1*scale, 3)
    ff.SetParNames("a", "b", "c", "f", "phi")
    ff.SetParLimits(2, 0.001, 10)  # force it to be non zero
    ff.SetParLimits(3, 0.03*scale, 10*scale)   # force it to be non zero

    for j in range(5):
        #fit = graph.Fit("ff","SVN")
        fit = graph.Fit("ff","SQN")
        if fit.CovMatrixStatus()==3 and fit.Chi2()/fit.Ndf() < 2: break

    fitStatus = -999
    fitStatus = fit.Status()

    ## Get parameters and stat error
    a = ff.GetParameter("a")
    aErr = ff.GetParError(ff.GetParNumber("a"))
    b = ff.GetParameter("b")
    bErr = ff.GetParError(ff.GetParNumber("b"))
    c = ff.GetParameter("c")
    cErr = ff.GetParError(ff.GetParNumber("c"))
    f = ff.GetParameter("f")
    fErr = ff.GetParError(ff.GetParNumber("f"))
    phi = ff.GetParameter("phi")
    phiErr = ff.GetParError(ff.GetParNumber("phi"))

    # Impose phi in [0, 2*pi[
    #        c > 0
    # for the purpose of comparing fit results of different vdm scans
    if c<0:
        c = -c
        phi = phi + np.pi
    phi = phi%(2*np.pi)

    fitParams = {
        "a"  : a,
        "b"  : b,
        "c"  : c,
        "f"  : f,
        "phi": phi,
        "aErr"  : aErr,
        "bErr"  : bErr,
        "cErr"  : cErr,
        "fErr"  : fErr,
        "phiErr": phiErr,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: a*x+b + c*np.sin(2*np.pi*f*x+phi)

    return(fitParams, fitFct)



def doAffineSineFit3steps(x0, y0, ye0, scale=1):

    ## Build TGraph
    N = len(y0)
    x = array("d", x0)
    xe = array("d", [0. for dummy in x0])
    y = array("d", y0)
    ye = array("d", ye0)
    graph = ROOT.TGraphErrors(N, x, y, xe, ye)

 
    ## First affine fit to get limits on a and b
    ff1 = ROOT.TF1("ff1","[0]*x+[1]")
    ff1.SetParNames("a", "b")
    for j in range(5):
        fit = graph.Fit("ff1","SQN")

    a0 = ff1.GetParameter("a")
    b0 = ff1.GetParameter("b")

    ymin = np.min(y0)
    yavg = np.mean(y0)
    ystd = np.std(y0)

    ## Affine + sine fit with limits
    ff2 = ROOT.TF1("ff2",str(a0)+"*x+"+str(b0)+" + ([0]*sin(2*pi*[1]*x+[2]))")
    ff2.SetParameters(0.2, 0.1*scale, 3)
    ff2.SetParNames("c", "f", "phi")
    ff2.SetParLimits(0, 0.001, 10)
    ff2.SetParLimits(1, 0.03*scale, 0.5*scale)

    for j in range(5):
        fit = graph.Fit("ff2","SQN")

    c0 = ff2.GetParameter("c")
    f0 = ff2.GetParameter("f")
    phi0 = ff2.GetParameter("phi")

    ## Affine + sine fit with limits
    ff = ROOT.TF1("ff","([0]*x+[1]) + ([2]*sin(2*pi*[3]*x+[4]))")
    ff.SetParameters(a0, b0, c0, f0, phi0)
    ff.SetParNames("a", "b", "c", "f", "phi")
    #ff.SetParLimits(2, 0.001, 1)
    #ff.SetParLimits(1, 0.01, 200)
    #ff.SetParLimits(3, 0.03, 2)
    C1 = 0.5
    C2 = 1.5
    d = 1.
    if a0>=0: ff.SetParLimits(0, C1*a0-d*scale, C2*a0+d*scale)
    else: ff.SetParLimits(0, C2*a0-d*scale, C1*a0+d*scale)
    if b0>=0: ff.SetParLimits(1, C1*b0-d, C2*b0+d)
    else: ff.SetParLimits(1, C2*b0-d, C1*b0+d)
    if c0>=0: ff.SetParLimits(2, C1*c0, C2*c0)
    else: ff.SetParLimits(2, C2*c0, C1*c0)
    if f0>=0: ff.SetParLimits(3, C1*f0, C2*f0)
    else: ff.SetParLimits(3, C2*f0, C1*f0)
    if phi0>=0: ff.SetParLimits(4, C1*phi0, C2*phi0)
    else: ff.SetParLimits(4, C2*phi0, C1*phi0)

    for j in range(5):
        #fit = graph.Fit("ff","SVN")
        fit = graph.Fit("ff","SQN")
        if fit.CovMatrixStatus()==3 and fit.Chi2()/fit.Ndf() < 2: break

    fitStatus = -999
    fitStatus = fit.Status()

    ## Get parameters and stat error
    a = ff.GetParameter("a")
    aErr = ff.GetParError(ff.GetParNumber("a"))
    b = ff.GetParameter("b")
    bErr = ff.GetParError(ff.GetParNumber("b"))
    c = ff.GetParameter("c")
    cErr = ff.GetParError(ff.GetParNumber("c"))
    f = ff.GetParameter("f")
    fErr = ff.GetParError(ff.GetParNumber("f"))
    phi = ff.GetParameter("phi")
    phiErr = ff.GetParError(ff.GetParNumber("phi"))

    # Impose phi in [0, 2*pi[
    #        c > 0
    # for the purpose of comparing fit results of different vdm scans
    if c<0:
        c = -c
        phi = phi + np.pi
    phi = phi%(2*np.pi)

    fitParams = {
        "a"  : a,
        "b"  : b,
        "c"  : c,
        "f"  : f,
        "phi": phi,
        "aErr"  : aErr,
        "bErr"  : bErr,
        "cErr"  : cErr,
        "fErr"  : fErr,
        "phiErr": phiErr,
        "chi2": fit.Chi2(),
        "ndof": fit.Ndf()
    }

    fitFct = lambda x: a*x+b + c*np.sin(2*np.pi*f*x+phi)

    return(fitParams, fitFct)



