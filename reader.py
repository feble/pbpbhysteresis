import csv
import json
import numpy as np
from scipy.stats import sem as sem

import utilities as ut


def getAvgBBDeflAtIP(vdmName, luminometer="PLT"):
    '''
    Reads Beam Beam deflection file procuded by the vdM FW
    from corr/BeamBeam_<det>_<fill>.json and returns the
    average beam beam deflection from 
 
    '''

    ## Get beam beam deflection at IP
    avgBBDefl = {}
    avgBBDefl_Err = {}

    BBfile = "data/BeamBeamDeflAtIP/BeamBeam_"+luminometer+"_"+vdmName+".json"
    with open(BBfile, 'r') as f:
        BBdata = json.load(f)

        for scan in list(BBdata.keys()):
            xy = ut.scan2xy(scan)
            avgBBDefl[xy] = []
            avgBBDefl_Err[xy] = []
            for istep in range(len(BBdata[scan])):
                # Average over BCIDs
                avgBBDefl[xy].append(np.mean(list(BBdata[scan][istep]["corr_"+xy+"coord"].values())))
                avgBBDefl_Err[xy].append(sem(list(BBdata[scan][istep]["corr_"+xy+"coord"].values())))
            avgBBDefl[xy] = np.array(avgBBDefl[xy])
            avgBBDefl_Err[xy] = np.array(avgBBDefl_Err[xy])

    return(avgBBDefl, avgBBDefl_Err)


def BBDeflAtDOROSPosition(BBDeflData, betaStar, Q, z=21.475):
    '''
    Multiplies the beam beam deflection at the IP by the factor
    taking into account the fact there is a deflection angle
    See Babaev_090620_updated.pdf

    '''

    BBDeflAtIP = BBDeflData[0]
    BBDeflAtIP_Err = BBDeflData[1]

    BBDeflAtDOROSPosition = {}
    BBDeflAtDOROSPosition_Err = {}
    for xy in list(BBDeflAtIP.keys()):
        betaStar_Err = 0.   # assuming 0 stat error, syst error is 5%
        f_bb = 1. + np.tan(np.pi*Q[xy])*z/betaStar
        f_bb_Err = np.tan(np.pi*Q[xy])*z * (betaStar_Err/betaStar**2)
        #f_bb = f_bb * 0.35
        #print(f_bb)
        BBDeflAtDOROSPosition[xy] = f_bb * BBDeflAtIP[xy]
        BBDeflAtDOROSPosition_Err[xy] = np.sqrt( (f_bb_Err * BBDeflAtIP[xy]) ** 2 + (f_bb * BBDeflAtIP_Err[xy]) ** 2 )
    return(BBDeflAtDOROSPosition, BBDeflAtDOROSPosition_Err)


def getOrbitDriftAtHeadOnOleksii(vdmName, oleksiiHOFilePath = "data/ODInterpolationData_Oleksii"):
    '''
    Reads Orbit Drift at head-on before, in the middle, after a given vdm scan
    for arcBPM and DOROS from Oleksii's orbit drift file

    '''

    ODatHO = {}

    fill = ut.getFillNumber(vdmName)
    vdmNumber = vdmName[3:]

    for BPM in ("DOROS", "arcBPM"):
        ODatHO[BPM] = {}
        HOFile = oleksiiHOFilePath + "/OrbitDrift_" + fill + "_" + BPM + ".json"
        with open(HOFile, 'r') as f:
            BPMdata = json.load(f)
            for dirr in ("X", "Y"):
                idx = BPMdata["Names"].index(dirr+str(vdmNumber))
                ODatHO[BPM][dirr] = {}
                ODatHO[BPM][dirr]["Timestamp"] = np.array(BPMdata["TimeWindows"][idx])
                for plane in ("H", "V"):
                    # Save as numpy array for manipulation convenience in the following
                    ODatHO[BPM][dirr][plane] = np.array(BPMdata["OrbitDrifts_"+ut.p2xy(plane)][idx])

    return(ODatHO)


def getOrbitDriftAtHeadOnDavid(vdmName, HOFile="data/ODInterpolationData_custom/DOROS.csv"):
    '''
    Reads head-on data before, in the middle, after a given vdm scan from
    David's head-on data for DOROS only (from DOROS BP per timestamp csv file)

    '''

    vdmName = vdmName.upper()

    ODatHO = { "Timestamp": [], "B1H": [], "B1V": [], "B2H": [], "B2V": [] }

    with open(HOFile, "r") as f:
        reader = csv.reader(f)
        HOData = list(reader)

        cols = HOData[0]
        ic_label = cols.index("Row Labels")
        ic_TimestampMin = cols.index("Min of timestamp")
        ic_TimestampMax = cols.index("Max of timestamp")
        ic_B1_H = cols.index("Average of B1_H")
        ic_B1_V = cols.index("Average of B1_V")
        ic_B2_H = cols.index("Average of B2_H")
        ic_B2_V = cols.index("Average of B2_V")
        #print(ic_B1_H, ic_B1_V, ic_B2_H, ic_B2_V)
        for row in HOData[1:]:
            if (row[ic_label].split('_')[0] == vdmName):
                ODatHO["Timestamp"].append( 0.5*(int(row[ic_TimestampMin])+int(row[ic_TimestampMax])) )
                ODatHO["B1H"].append( float(row[ic_B1_H]) )
                ODatHO["B1V"].append( float(row[ic_B1_V]) )
                ODatHO["B2H"].append( float(row[ic_B2_H]) )
                ODatHO["B2V"].append( float(row[ic_B2_V]) )

        # Convert into numpy array for manipulation convenience in the following
        for label in ("Timestamp", "B1H", "B1V", "B2H", "B2V"):
            ODatHO[label] = np.array(ODatHO[label])

        if (len(ODatHO["Timestamp"])!=5):
            print("ERROR: Less than 5 points for interpolation")
            print("Exit")
            sys.exit()

    return(ODatHO)


def getOrbitDriftAtHeadOnCustom(vdmName, BPMList, ODdata, timestampFile="data/ODInterpolationData_custom/timestamps.json"):
    '''
    Compute time average beam positions before, in the middle and after each X/Y scan
    during timestamp interval defined in the timestamp file

    '''

    ## Open timestamp file
    with open(timestampFile, 'r') as f:
        timestampData = json.load(f)
    
    timestampDataVdm = timestampData[vdmName]


    ## Compute beam positions at head on
    ODatHO = {}
    #for BPM in ("DOROS", "arcBPM"):
    for BPM in BPMList:

        # Timestamp,B1_H_L,B1_H_R,B1_V_L,B1_V_R,B2_H_L,B2_H_R,B2_V_L,B2_V_R
        cols = ODdata[BPM][0]
        ic_Timestamp = cols.index("timestamp")
        ic = {
            "B1H": cols.index("B1_H"),
            "B1V": cols.index("B1_V"),
            "B2H": cols.index("B2_H"),
            "B2V": cols.index("B2_V")
        }

        ODatHO[BPM] = {}

        # Initialise
        for scan in list(timestampDataVdm.keys()):
            ODatHO[BPM][scan] = {}
            for BP in ("B1H", "B1V", "B2H", "B2V", "Timestamp"):
                ODatHO[BPM][scan][BP] = []

        for scan in list(timestampDataVdm.keys()):
            for tsData in timestampDataVdm[scan]:
                ODatHO[BPM][scan]["Timestamp"].append( 0.5 * (tsData[0] + tsData[1]) )
                ODatHOList = {}
                for BP in ("B1H", "B1V", "B2H", "B2V"): ODatHOList[BP] = []
                for row in ODdata[BPM][1:]:
                    ts = int(row[ic_Timestamp])
                    # Check if timestamp is whithin the timestamp window
                    if ts > tsData[1]: break
                    elif ts >= tsData[0] and ts <= tsData[1]:
                        for BP in ("B1H", "B1V", "B2H", "B2V"):
                            if BPM == "DOROS":
                                ODatHOList[BP].append( 0.001 * float(row[ic[BP]]) )
                            elif BPM == "arc":
                                # No nomSep as nomSep=0 as we're looking at head-on
                                #ODatHOList[BP].append( 0.001 * float(row[ic[BP]]) + 0.5 * sign[BP] * nomSep[fscan][fstep][2] )
                                ODatHOList[BP].append( 0.001 * float(row[ic[BP]]) )
                            else:
                                print("ERROR: Unknown BPM %s" %bpm)
                                print("Exit")
                                sys.exit()
                for BP in ("B1H", "B1V", "B2H", "B2V"):
                    ODatHO[BPM][scan][BP].append(np.mean(ODatHOList[BP]))
            for BP in ("B1H", "B1V", "B2H", "B2V", "Timestamp"):
                ODatHO[BPM][scan][BP] = np.array(ODatHO[BPM][scan][BP])

    return(ODatHO)



def getBeamPosSep(vdmName, scanFile, ODFiles, LSCFile, HOFile, killListTimestamp, tunes, betaStar):
    '''
    Reads Beam Position Measurement data for a given scan and returns a dictionary
    with beam positions and separations in the X and Y scans

    Syntax for dictionary keys:
    <variable(Err)>.<algo>.<granularity>_<BPM>_<correction>
    Where:
      * algo = avg: average during a scan step
               interpolation1D: linear interpolation using 3 points (before, middle, after scan)
                                using beam position in the scanning direction
      * granularity = perSP, perScan
      * BPM = DOROS, arc
      * correction = LSC

    e.g. B1H.avg.perSP_DOROS_LSC
    When not applicable, do not specify field, e.g. sepNom

    The dictionary returned has the following keys:
        sepNom


        *** DOROS and arc BPMs measurements ***

        B1H.avg.perSP_DOROS
        B1V.avg.perSP_DOROS
        B2H.avg.perSP_DOROS
        B2V.avg.perSP_DOROS
        B1HErr.avg.perSP_DOROS
        B1VErr.avg.perSP_DOROS
        B2HErr.avg.perSP_DOROS
        B2VErr.avg.perSP_DOROS
        B1H.avg.perSP_arc
        B1V.avg.perSP_arc
        B2H.avg.perSP_arc
        B2V.avg.perSP_arc
        B1HErr.avg.perSP_arc
        B1VErr.avg.perSP_arc
        B2HErr.avg.perSP_arc
        B2VErr.avg.perSP_arc

        B1H.avg.perSP_DOROS_LSC
        B1V.avg.perSP_DOROS_LSC
        B2H.avg.perSP_DOROS_LSC
        B2V.avg.perSP_DOROS_LSC
        B1HErr.avg.perSP_DOROS_LSC
        B1VErr.avg.perSP_DOROS_LSC
        B2HErr.avg.perSP_DOROS_LSC
        B2VErr.avg.perSP_DOROS_LSC
        B1H.avg.perSP_arc_LSC
        B1V.avg.perSP_arc_LSC
        B2H.avg.perSP_arc_LSC
        B2V.avg.perSP_arc_LSC
        B1HErr.avg.perSP_arc_LSC
        B1VErr.avg.perSP_arc_LSC
        B2HErr.avg.perSP_arc_LSC
        B2VErr.avg.perSP_arc_LSC

        sep.avg.perSP_DOROS
        sepErr.avg.perSP_DOROS
        sep.avg.perSP_arc
        sepErr.avg.perSP_arc
        sep.avg.perSP_DOROS_LSC
        sepErr.avg.perSP_DOROS_LSC
        sep.avg.perSP_arc_LSC
        sepErr.avg.perSP_arc_LSC


        *** Beam Beam Deflection ***

        BBDefl
        BBDeflErr
        BBDeflAtDOROS
        BBDeflATDOROSErr


        *** Orbit Drift ***

        from custom file
        from Oleksii's file if explicitely mentioned

        ** In scanning direction **

        * Beam positions *

        ODB1H.interpolation1D3pts.perSP_DOROS
        ODB1V.interpolation1D3pts.perSP_DOROS
        ODB2H.interpolation1D3pts.perSP_DOROS
        ODB2V.interpolation1D3pts.perSP_DOROS
        ODB1H.interpolation1D3pts.perSP_DOROS_LSC
        ODB1V.interpolation1D3pts.perSP_DOROS_LSC
        ODB2H.interpolation1D3pts.perSP_DOROS_LSC
        ODB2V.interpolation1D3pts.perSP_DOROS_LSC

        ODB1H.interpolation1D2pts.perSP_DOROS
        ODB1V.interpolation1D2pts.perSP_DOROS
        ODB2H.interpolation1D2pts.perSP_DOROS
        ODB2V.interpolation1D2pts.perSP_DOROS
        ODB1H.interpolation1D2pts.perSP_DOROS_LSC
        ODB1V.interpolation1D2pts.perSP_DOROS_LSC
        ODB2H.interpolation1D2pts.perSP_DOROS_LSC
        ODB2V.interpolation1D2pts.perSP_DOROS_LSC

        ODB1H.interpolation1D3pts.perSP_arc
        ODB1V.interpolation1D3pts.perSP_arc
        ODB2H.interpolation1D3pts.perSP_arc
        ODB2V.interpolation1D3pts.perSP_arc
        ODB1H.interpolation1D3pts.perSP_arc_LSC
        ODB1V.interpolation1D3pts.perSP_arc_LSC
        ODB2H.interpolation1D3pts.perSP_arc_LSC
        ODB2V.interpolation1D3pts.perSP_arc_LSC

        ODB1H.interpolation1D2pts.perSP_arc
        ODB1V.interpolation1D2pts.perSP_arc
        ODB2H.interpolation1D2pts.perSP_arc
        ODB2V.interpolation1D2pts.perSP_arc
        ODB1H.interpolation1D2pts.perSP_arc_LSC
        ODB1V.interpolation1D2pts.perSP_arc_LSC
        ODB2H.interpolation1D2pts.perSP_arc_LSC
        ODB2V.interpolation1D2pts.perSP_arc_LSC

        * Beam separations *

        ODsepH.interpolation1D3pts.perSP_DOROS
        ODsepV.interpolation1D3pts.perSP_DOROS
        ODsepH.interpolation1D2pts.perSP_DOROS
        ODsepV.interpolation1D2pts.perSP_DOROS
        ODsepH.interpolation1D3pts.perSP_arc
        ODsepV.interpolation1D3pts.perSP_arc
        ODsepH.interpolation1D2pts.perSP_arc
        ODsepV.interpolation1D2pts.perSP_arc

        ODsepH.interpolation1D3pts.perSP_DOROS_LSC
        ODsepV.interpolation1D3pts.perSP_DOROS_LSC
        ODsepH.interpolation1D2pts.perSP_DOROS_LSC
        ODsepV.interpolation1D2pts.perSP_DOROS_LSC
        ODsepH.interpolation1D3pts.perSP_arc_LSC
        ODsepV.interpolation1D3pts.perSP_arc_LSC
        ODsepH.interpolation1D2pts.perSP_arc_LSC
        ODsepV.interpolation1D2pts.perSP_arc_LSC

        ODsepH.interpolation1D3ptsOleksii.perSP_DOROS
        ODsepV.interpolation1D3ptsOleksii.perSP_DOROS
        ODsepH.interpolation1D2ptsOleksii.perSP_DOROS
        ODsepV.interpolation1D2ptsOleksii.perSP_DOROS
        ODsepH.interpolation1D3ptsOleksii.perSP_arc
        ODsepV.interpolation1D3ptsOleksii.perSP_arc
        ODsepH.interpolation1D2ptsOleksii.perSP_arc
        ODsepV.interpolation1D2ptsOleksii.perSP_arc

        ODsepH.interpolation1D3ptsOleksii.perSP_DOROS_LSC
        ODsepV.interpolation1D3ptsOleksii.perSP_DOROS_LSC
        ODsepH.interpolation1D2ptsOleksii.perSP_DOROS_LSC
        ODsepV.interpolation1D2ptsOleksii.perSP_DOROS_LSC
        ODsepH.interpolation1D3ptsOleksii.perSP_arc_LSC
        ODsepV.interpolation1D3ptsOleksii.perSP_arc_LSC
        ODsepH.interpolation1D2ptsOleksii.perSP_arc_LSC
        ODsepV.interpolation1D2ptsOleksii.perSP_arc_LSC


        *** In non scanning direction ***

        offsetSepH.interpolation1D2pts.perScan_DOROS
        offsetSepV.interpolation1D2pts.perScan_DOROS
        offsetSepH.interpolation1D2pts.perScan_arc
        offsetSepV.interpolation1D2pts.perScan_arc
        offsetSepH.interpolation1D2pts.perScan_DOROS_LSC
        offsetSepV.interpolation1D2pts.perScan_DOROS_LSC
        offsetSepH.interpolation1D2ptsOleksii.perScan_DOROS
        offsetSepV.interpolation1D2ptsOleksii.perScan_DOROS
        offsetSepH.interpolation1D2ptsOleksii.perScan_arc
        offsetSepV.interpolation1D2ptsOleksii.perScan_arc
        WARNING: offsetSepH.interpolation1D2pts.perScan_DOROS["X"]
                 is not an offset in the non scanning direction
                 but in the scanning direction!

        
        sep.interpolation1DfromOleksiisFile.perSP_DOROS
        sep.interpolation1DfromOleksiisFile.perSP_arc


    '''


    ## Book dictionary to store beam positions and separations
    beamPosSep = {}

    # Temporary dictionary that will not be returned
    beamPosSepTmp = { "nomSep": {} }


    ###########################
    ##  READ DATA FROM FILES
    ###########################

    ## Get separation from arc and DOROS interpolation from Oleksii's files
    orbitDriftAtHeadOn = None
    orbitDriftAtHeadOnDavid = None

    with open("data/sep_DOROS_interpolation1D.json", 'r') as f:
        DOROSInterpolationSep = json.load(f)
    with open("data/sep_arc_interpolation1D.json", 'r') as f:
        arcInterpolationSep = json.load(f)

    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_DOROS"] = {}
    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_DOROS"]["X"] = np.array( DOROSInterpolationSep[vdmName]["X"] )
    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_DOROS"]["Y"] = np.array( DOROSInterpolationSep[vdmName]["Y"] )

    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_arc"] = {}
    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_arc"]["X"] = np.array( arcInterpolationSep[vdmName]["X"] )
    beamPosSep["sep.interpolation1DfromOleksiisFile.perSP_arc"]["Y"] = np.array( arcInterpolationSep[vdmName]["Y"] )


    ## Get per timestamp OD
    ODdata = {}
    BPMList = list(ODFiles.keys())
    for BPM in BPMList:
        with open(ODFiles[BPM], "r") as f:
            reader = csv.reader(f)
            ODdata[BPM] = list(reader)


    ## Get Orbit Drift at Head On
    #  from David's Head On file
    #orbitDriftAtHeadOn = getOrbitDriftAtHeadOnDavid(vdmName)
    #  from Oleksii's file for the vdM FW
    orbitDriftAtHeadOnOleksii = getOrbitDriftAtHeadOnOleksii(vdmName)
    #  from timestamp by recomputing OD per beam from OD file
    orbitDriftAtHeadOn = getOrbitDriftAtHeadOnCustom(vdmName, BPMList, ODdata)


    ## Get Length Scale Calibration
    with open(LSCFile, 'r') as f:
        LSCInfo = json.load(f)
    # If forward and backward only are given, take their average
    # Else remove "_" from BP name
    for BP in ("B1_H", "B1_V", "B2_H", "B2_V"):
        if BP not in list(LSCInfo.keys()):
            LSCInfo[BP.replace("_","")] = 0.5 * (LSCInfo[BP+"_fw"]+LSCInfo[BP+"_bw"])
        else:
            LSCInfo[BP.replace("_","")] = LSCInfo.pop(BP)
    #print("LSC_B1_H: ",LSCInfo["B1H"], "\nLSC_B2_H: ",LSCInfo["B2H"], "\nLSC_B1_V: ",LSCInfo["B1V"], "\nLSC_B2_V: ",LSCInfo["B2V"])


    ## Get beam beam deflection contribution to DOROS beam position measurement
    beamPosSep["BBDefl"], beamPosSep["BBDeflErr"] = getAvgBBDeflAtIP(vdmName)
    beamPosSep["BBDeflAtDOROS"], beamPosSep["BBDeflAtDOROSErr"] = BBDeflAtDOROSPosition((beamPosSep["BBDefl"], beamPosSep["BBDeflErr"]), betaStar, tunes)

            
    ## Get vdm scan data
    with open(scanFile, 'r') as f:
        scanInfo = json.load(f)

    beamPosSep["timeOrigin"] = {}
    for i, name in enumerate(scanInfo["ScanNames"]):
        key = "Scan_" + str(i+1)
        xy = ut.scan2xy(key)
        if key not in list(beamPosSepTmp["nomSep"].keys()): beamPosSepTmp["nomSep"][key] = {}
        scanpoints = scanInfo[key]
        for j, sp in enumerate(scanpoints):
            ## Get sepration info for this step
            # -3 +3 to restrict time window to remove
            # outliers at begining and end of scan step
            beamPosSepTmp["nomSep"][key]["step"+str(j+1)] = [int(sp[3])+3, int(sp[4])-3, float(sp[5])]
            if sp[2] == 1: beamPosSep["timeOrigin"][xy] = 0.5*(beamPosSepTmp["nomSep"][key]["step"+str(j+1)][0] + beamPosSepTmp["nomSep"][key]["step"+str(j+1)][1])


    orderedScans = [ scan for scan in sorted(list(beamPosSepTmp["nomSep"].keys()), key= lambda a: int(a[5])) ]
    orderedSteps = []
    scan = list(beamPosSepTmp["nomSep"].keys())[0]
    orderedSteps.append( [ step for step in sorted(list(beamPosSepTmp["nomSep"][scan].keys()), key= lambda a: int(a[4:])) ] )
    scan = list(beamPosSepTmp["nomSep"].keys())[1]
    orderedSteps.append( [ step for step in sorted(list(beamPosSepTmp["nomSep"][scan].keys()), key= lambda a: int(a[4:])) ] )



    ##################################################################
    ##  COMPUTE ORBIT DRIFT IN SCANNING AND NON SCANNING DIRECTIONS
    ##################################################################

    ## Compute OD in non scanning direction (offset) and scanning direction
    for BPM in ("DOROS", "arcBPM"):
       if BPM == "arcBPM": BPMvar = "arc"
       else: BPMvar = BPM

        ## Compute offsets
       for plane in ["H", "V"]:
           #beamPosSep["offsetSep"+plane+".interpolation1D2pts.perScan_"+BPMvar] = {}
           beamPosSep["offsetSep"+plane+".interpolation1D2ptsOleksii.perScan_"+BPMvar] = {}
           for xy in ["X", "Y"]:
               #beamPosSep["offsetSep"+plane+".interpolation1D2pts.perScan_"+BPMvar][xy] = np.mean(orbitDriftAtHeadOn[BPM][xy][plane])
               beamPosSep["offsetSep"+plane+".interpolation1D2ptsOleksii.perScan_"+BPMvar][xy] = np.mean(orbitDriftAtHeadOnOleksii[BPM][xy][plane][[0,-1]])

       for iplane, plane in enumerate(("H", "V")):
           beamPosSep["ODsep"+plane+".interpolation1D2ptsOleksii.perSP_"+BPMvar] = {}
           for iscan, scan in enumerate(orderedScans):
               xy = ut.scan2xy(scan)
               beamPosSep["ODsep"+plane+".interpolation1D2ptsOleksii.perSP_"+BPMvar][xy] = []
               for step in orderedSteps[iscan]:
                   avgts = 0.5*(beamPosSepTmp["nomSep"][scan][step][0]+beamPosSepTmp["nomSep"][scan][step][1])
                   beamPosSep["ODsep"+plane+".interpolation1D2ptsOleksii.perSP_"+BPMvar][xy].append( orbitDriftAtHeadOnOleksii[BPM][xy][plane][0] + ((orbitDriftAtHeadOnOleksii[BPM][xy][plane][2] - orbitDriftAtHeadOnOleksii[BPM][xy][plane][0]) / (orbitDriftAtHeadOnOleksii[BPM][xy]["Timestamp"][2] - orbitDriftAtHeadOnOleksii[BPM][xy]["Timestamp"][0])) * (avgts - orbitDriftAtHeadOnOleksii[BPM][xy]["Timestamp"][0]) )

               beamPosSep["ODsep"+plane+".interpolation1D2ptsOleksii.perSP_"+BPMvar][xy] = np.array(beamPosSep["ODsep"+plane+".interpolation1D2ptsOleksii.perSP_"+BPMvar][xy])



    ## Compute OD in non scanning direction (offset) and scanning direction
    for BPM in BPMList:
        if BPM == "arcBPM": BPMvar = "arc"
        else: BPMvar = BPM

        ## Initialise dictionaries
        for plane in ["H", "V"]:
            beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPMvar] = {}
            beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"] = {}

        for BP in ("B1H", "B1V", "B2H", "B2V"):
            ## Initialise dictionaries
            beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar] = {}
            beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"] = {}

        for iscan, scan in enumerate(orderedScans):
            xy = ut.scan2xy(scan)
            for BP in ("B1H", "B1V", "B2H", "B2V"):
                beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar][xy] = []
                for step in orderedSteps[iscan]:
                   avgts = 0.5*(beamPosSepTmp["nomSep"][scan][step][0]+beamPosSepTmp["nomSep"][scan][step][1])
                   beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar][xy].append( orbitDriftAtHeadOn[BPM][xy][BP][0] + ((orbitDriftAtHeadOn[BPM][xy][BP][2] - orbitDriftAtHeadOn[BPM][xy][BP][0]) / (orbitDriftAtHeadOn[BPM][xy]["Timestamp"][2] - orbitDriftAtHeadOn[BPM][xy]["Timestamp"][0])) * (avgts - orbitDriftAtHeadOn[BPM][xy]["Timestamp"][0]) )
                beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar][xy] = np.array(beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar][xy])
                beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"][xy] = beamPosSep["OD"+BP+".interpolation1D2pts.perSP_"+BPMvar][xy] / LSCInfo[BP]

            for plane in ("H", "V"):
                beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPMvar][xy] = beamPosSep["ODB1"+plane+".interpolation1D2pts.perSP_"+BPMvar][xy] - beamPosSep["ODB2"+plane+".interpolation1D2pts.perSP_"+BPMvar][xy]
                beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"][xy] = beamPosSep["ODB1"+plane+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"][xy] - beamPosSep["ODB2"+plane+".interpolation1D2pts.perSP_"+BPMvar+"_LSC"][xy]

                #print(beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPM][xy])
                #print(beamPosSep["ODsep"+plane+".interpolation1D2pts.perSP_"+BPM+"_LSC"][xy])


    ## Calculate OD per scan step from interpolation
    #  Code for David's Head On file
    #if orbitDriftAtHeadOnDavid != None:
    #    for BP in ("B1H", "B1V", "B2H", "B2V"):
    #        beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"] = {}
    #        for iscan, scan in enumerate(orderedScans):
    #            xy = ut.scan2xy(scan)
    #            beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"][xy] = []
    #            for step in orderedSteps[iscan]:
    #                if scan == "Scan_1": idx1 = 0
    #                else: idx1 = 2
    #                if int(step[4:])<=7: idx2 = 0
    #                else: idx2 = 1
    #                avgts = 0.5*(beamPosSepTmp["nomSep"][scan][step][0]+beamPosSepTmp["nomSep"][scan][step][1])
    #                beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"][xy].append( orbitDriftAtHeadOnDavid[BP][idx1+idx2] + ((orbitDriftAtHeadOnDavid[BP][idx1+idx2+1] - orbitDriftAtHeadOnDavid[BP][idx1+idx2]) / (orbitDriftAtHeadOnDavid["Timestamp"][idx1+idx2+1] - orbitDriftAtHeadOnDavid["Timestamp"][idx1+idx2])) * (avgts - orbitDriftAtHeadOnDavid["Timestamp"][idx1+idx2]) )
    #            beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"][xy] = np.array(beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"][xy])
    #            beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS_LSC"][xy] = np.array(beamPosSep["OD"+BP+".interpolation1DDavid.perSP_DOROS"][xy]) / LSCInfo[BP]

    #    for iplane, plane in enumerate(("H", "V")):
    #        for scan in orderedScans:
    #            xy = ut.scan2xy(scan)
    #            beamPosSep["ODsep"+plane+".interpolation1DDavid.perSP_DOROS"][xy] = beamPosSep["ODB1"+plane+".interpolation1DDavid.perSP_DOROS"][xy] \
    #                                                                         - beamPosSep["ODB2"+plane+".interpolation1DDavid.perSP_DOROS"][xy]
    #            beamPosSep["ODsep"+plane+".interpolation1DDavid.perSP_DOROS_LSC"][xy] = beamPosSep["ODB1"+plane+".interpolation1DDavid.perSP_DOROS_LSC"][xy] \
    #                                                                             - beamPosSep["ODB2"+plane+".interpolation1DDavid.perSP_DOROS_LSC"][xy]




    ############################################
    ##  COMPUTE BEAM POSITIONS AND SEPARATION
    ############################################

    ## Compute beam positions per scan step
    #  Get beam positions per timestamp
    for BPM in BPMList:
        #  Initialise beam positions lists
        for BP in ("B1H", "B1V", "B2H", "B2V"):
            beamPosSepTmp[BP+"L_"+BPM] = {}
            for iscan, scan in enumerate(orderedScans):
                beamPosSepTmp[BP+"L_"+BPM][scan]  = {}
                for step in orderedSteps[iscan]:
                    beamPosSepTmp[BP+"L_"+BPM][scan][step] = []


        # Timestamp,B1_H_L,B1_H_R,B1_V_L,B1_V_R,B2_H_L,B2_H_R,B2_V_L,B2_V_R
        cols = ODdata[BPM][0]
        ic_Timestamp = cols.index("timestamp")
        #if BPM == "arc":
        #    ic = {
        #        "B1H": cols.index("B1_H_diff_B1_H"),
        #        "B1V": cols.index("B1_V_diff_B1_V"),
        #        "B2H": cols.index("B2_H_diff_B2_H"),
        #        "B2V": cols.index("B2_V_diff_B2_V")
        #    }
        #else:
        #    ic = {
        #        #"B1HL": cols.index("B1_H_L"),
        #        #"B1VL": cols.index("B1_V_L"),
        #        #"B2HL": cols.index("B2_H_L"),
        #        #"B2VL": cols.index("B2_V_L"),
        #        #"B1HR": cols.index("B1_H_R"),
        #        #"B1VR": cols.index("B1_V_R"),
        #        #"B2HR": cols.index("B2_H_R"),
        #        #"B2VR": cols.index("B2_V_R")
        #        "B1H": cols.index("B1_H"),
        #        "B1V": cols.index("B1_V"),
        #        "B2H": cols.index("B2_H"),
        #        "B2V": cols.index("B2_V")
        #    }

        ic = {
            "B1H": cols.index("B1_H"),
            "B1V": cols.index("B1_V"),
            "B2H": cols.index("B2_H"),
            "B2V": cols.index("B2_V")
        }
        sign = {
            "B1H": +1,
            "B1V": +1,
            "B2H": -1,
            "B2V": -1,
        }
        for row in ODdata[BPM][1:]:
            ts = int(row[ic_Timestamp])
            # Check if timestamp is whithin a scan step
            found, fscan, fstep = ut.isInSP(ts, beamPosSepTmp["nomSep"], killListTimestamp)
            if found:
                for BP in ("B1H", "B1V", "B2H", "B2V"):
                    #if BPM == "DOROS":
                    #    beamPosSepTmp[BP+"L_"+BPM][fscan][fstep].append( 0.001 * float(row[ic[BP]]) )
                    #elif BPM == "arc":
                    #    beamPosSepTmp[BP+"L_"+BPM][fscan][fstep].append( -0.001 * float(row[ic[BP]]) )
                    if BPM == "DOROS":
                        #beamPosSepTmp[BP+"L_"+BPM][fscan][fstep].append( 0.5 * ( float(row[ic[BP+"L"]]) + float(row[ic[BP+"R"]]) ) )
                        beamPosSepTmp[BP+"L_"+BPM][fscan][fstep].append( 0.001 * float(row[ic[BP]]) )
                    elif BPM == "arc":
                        beamPosSepTmp[BP+"L_"+BPM][fscan][fstep].append( 0.001 * float(row[ic[BP]]) + 0.5 * sign[BP] * beamPosSepTmp["nomSep"][fscan][fstep][2] )
                    else:
                        print("ERROR: Unknown BPM %s" %bpm)
                        print("Exit")
                        sys.exit()


        #  Remove outliers
        #  e.g. Data close to 
        #print("Fraction kept:")
        for iscan, scan in enumerate(orderedScans):
            for step in orderedSteps[iscan]:
                for BP in ("B1H", "B1V", "B2H", "B2V"):
                    n0 = len(beamPosSepTmp[BP+"L_"+BPM][scan][step])
                    beamPosSepTmp[BP+"L_"+BPM][scan][step] = ut.removeOutliers(beamPosSepTmp[BP+"L_"+BPM][scan][step])
                    n = len(beamPosSepTmp[BP+"L_"+BPM][scan][step])
                    #print("\t%s %s %s: %.2f" %(BP, scan, step, float(n1)/n0))

        # Compute average beam positions
        for BP in ("B1H", "B1V", "B2H", "B2V"):
            BPvar = BP + ".avg.perSP_" + BPM
            BPvarErr = BP + "Err.avg.perSP_" + BPM
            BPvarLSC = BPvar + "_LSC"
            BPvarErrLSC = BPvarErr + "_LSC"
            beamPosSep[BPvar] = {}
            beamPosSep[BPvarErr] = {}
            beamPosSep[BPvarLSC] = {}
            beamPosSep[BPvarErrLSC] = {}
            for iscan, scan in enumerate(orderedScans):
                xy = ut.scan2xy(scan)
                beamPosSep[BPvar][xy] = []
                beamPosSep[BPvarErr][xy] = []
                for step in orderedSteps[iscan]:
                    if len(beamPosSepTmp[BP+"L_"+BPM][scan][step]) == 0:
                        print("No BPM data for %s and %s" %(scan, step))
                    beamPosSep[BPvar][xy].append(np.mean(beamPosSepTmp[BP+"L_"+BPM][scan][step]))
                    beamPosSep[BPvarErr][xy].append(sem(beamPosSepTmp[BP+"L_"+BPM][scan][step]))
                beamPosSep[BPvar][xy] = np.array(beamPosSep[BPvar][xy])
                beamPosSep[BPvarErr][xy] = np.array(beamPosSep[BPvarErr][xy])
                beamPosSep[BPvarLSC][xy] = beamPosSep[BPvar][xy][:] / LSCInfo[BP]
                beamPosSep[BPvarErrLSC][xy] = beamPosSep[BPvarErr][xy][:] / LSCInfo[BP]


        ## Compute average beam separations
        beamPosSep["sepNom"] = {}
        beamPosSep["sepNom"]["X"] = np.array([ beamPosSepTmp["nomSep"]["Scan_1"][step][2] for step in orderedSteps[0] ])
        beamPosSep["sepNom"]["Y"] = np.array([ beamPosSepTmp["nomSep"]["Scan_2"][step][2] for step in orderedSteps[1] ])

        beamPosSep["timestamp.avg.perSP"] = {}
        step0 = orderedSteps[0][0]
        timeOrigin = 0.5*(beamPosSepTmp["nomSep"]["Scan_1"][step0][0]+beamPosSepTmp["nomSep"]["Scan_1"][step0][1])
        beamPosSep["timestamp.avg.perSP"]["X"] = np.array([ \
            0.5*(beamPosSepTmp["nomSep"]["Scan_1"][step][0]+beamPosSepTmp["nomSep"]["Scan_1"][step][1]) for step in orderedSteps[0] ]) \
            - beamPosSep["timeOrigin"]["X"]
        beamPosSep["timestamp.avg.perSP"]["Y"] = np.array([ \
            0.5*(beamPosSepTmp["nomSep"]["Scan_2"][step][0]+beamPosSepTmp["nomSep"]["Scan_2"][step][1]) for step in orderedSteps[1] ]) \
            - beamPosSep["timeOrigin"]["X"]

    for BPM in BPMList:
        beamPosSep["sep.avg.perSP_"+BPM] = {}
        beamPosSep["sep.avg.perSP_"+BPM]["X"] = beamPosSep["B1H.avg.perSP_"+BPM]["X"] - beamPosSep["B2H.avg.perSP_"+BPM]["X"]
        beamPosSep["sep.avg.perSP_"+BPM]["Y"] = beamPosSep["B1V.avg.perSP_"+BPM]["Y"] - beamPosSep["B2V.avg.perSP_"+BPM]["Y"]
        beamPosSep["sepErr.avg.perSP_"+BPM] = {}
        beamPosSep["sepErr.avg.perSP_"+BPM]["X"] = np.sqrt( (beamPosSep["B1HErr.avg.perSP_"+BPM]["X"])**2 + (beamPosSep["B2HErr.avg.perSP_"+BPM]["X"])**2 )
        beamPosSep["sepErr.avg.perSP_"+BPM]["Y"] = np.sqrt( (beamPosSep["B1VErr.avg.perSP_"+BPM]["Y"])**2 + (beamPosSep["B2VErr.avg.perSP_"+BPM]["Y"])**2 )

        # Temporarily only for DOROS as LSC arc data is not available
        if BPM == "DOROS":
            beamPosSep["sep.avg.perSP_"+BPM+"_LSC"] = {}
            beamPosSep["sep.avg.perSP_"+BPM+"_LSC"]["X"] = beamPosSep["B1H.avg.perSP_"+BPM+"_LSC"]["X"] - beamPosSep["B2H.avg.perSP_"+BPM+"_LSC"]["X"]
            beamPosSep["sep.avg.perSP_"+BPM+"_LSC"]["Y"] = beamPosSep["B1V.avg.perSP_"+BPM+"_LSC"]["Y"] - beamPosSep["B2V.avg.perSP_"+BPM+"_LSC"]["Y"]
            beamPosSep["sepErr.avg.perSP_"+BPM+"_LSC"] = {}
            beamPosSep["sepErr.avg.perSP_"+BPM+"_LSC"]["X"] = np.sqrt( beamPosSep["B1HErr.avg.perSP_"+BPM+"_LSC"]["X"]**2 + beamPosSep["B2HErr.avg.perSP_"+BPM+"_LSC"]["X"]**2 )
            beamPosSep["sepErr.avg.perSP_"+BPM+"_LSC"]["Y"] = np.sqrt( beamPosSep["B1VErr.avg.perSP_"+BPM+"_LSC"]["Y"]**2 + beamPosSep["B2VErr.avg.perSP_"+BPM+"_LSC"]["Y"]**2 )
        elif BPM == "arc":
            beamPosSep["sep.avg.perSP_"+BPM+"_LSC"] = beamPosSep["sep.avg.perSP_"+BPM].copy()
            beamPosSep["sepErr.avg.perSP_"+BPM+"_LSC"] = beamPosSep["sepErr.avg.perSP_"+BPM].copy()



    ## Make csv file with all the separation data
    makecsvTable = False
    if (makecsvTable):
        csvtable = []
        row = [ "scan", "step", "nominal", "arc_interpolation", "DOROS_interpolation", "DOROSperSP", "DOROSperSP_LSC" ]
        csvtable.append(row)
        for iscan, scan in enumerate(orderedScans):
            if (iscan==0): dirr="X"
            elif (iscan==1): dirr="Y"
            else: print("Exit!!!!"); sys.exit()
            for istep, step in enumerate(orderedSteps):
                row = [ dirr, istep+1, beamPosSepTmp["nomSep"][scan][step][2],
                        arcInterpolationSep[vdmName][dirr][istep], DOROSInterpolationSep[vdmName][dirr][istep],
                        beamPosSep["sep.avg.perSP_DOROS"][dirr][istep], beamPosSep["sep.avg.perSP_DOROS_LSC"][dirr][istep] ]
                csvtable.append(row)

        fileName = "data/separationFiles/"+vdmName+"separations.csv"
        csvfile = open(fileName, 'w')
        writer = csv.writer(csvfile)
        writer.writerows(csvtable)
        csvfile.close()
        print("%s has been written" %fileName)


    ## Write new separation to new file
    makeSeparationFile = False
    if (makeSeparationFile):
        for i, name in enumerate(scanInfo["ScanNames"]):
            key = "Scan_" + str(i+1)
            scanpoints = scanInfo[key]
            for j, sp in enumerate(scanpoints):
                if key=="Scan_1":
                    scanInfo[key][j][5] = beamPosSep["sepDOROSperSPLSC_minusBBDefl"]["X"][j]
                    scanInfo[key][j][6] = beamPosSep["sepDOROSperSPLSC_minusBBDefl"]["X"][j]
                elif key=="Scan_2":
                    scanInfo[key][j][5] = beamPosSep["sepDOROSperSPLSC_minusBBDefl"]["Y"][j]
                    scanInfo[key][j][7] = beamPosSep["sepDOROSperSPLSC_minusBBDefl"]["Y"][j]
                #if key=="Scan_1":
                #    scanInfo[key][j][5] = DOROSInterpolationSep[vdmName]["X"][j]
                #    scanInfo[key][j][6] = DOROSInterpolationSep[vdmName]["X"][j]
                #elif key=="Scan_2":
                #    scanInfo[key][j][5] = DOROSInterpolationSep[vdmName]["Y"][j]
                #    scanInfo[key][j][7] = DOROSInterpolationSep[vdmName]["Y"][j]
                #if key=="Scan_1":
                #    scanInfo[key][j][5] = arcInterpolationSep[vdmName]["X"][j]
                #    scanInfo[key][j][6] = arcInterpolationSep[vdmName]["X"][j]
                #elif key=="Scan_2":
                #    scanInfo[key][j][5] = arcInterpolationSep[vdmName]["Y"][j]
                #    scanInfo[key][j][7] = arcInterpolationSep[vdmName]["Y"][j]

        fileName = scanFile.split(".")[0]+"_DOROS_LSC_best_minusBBDefl"+".json"
        with open(fileName, 'w') as f:
            json.dump(scanInfo, f)
        print(fileName+" has been written")

    #for k in list(beamPosSep.keys()): print( k )

    return( beamPosSep )


